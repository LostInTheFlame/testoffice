﻿namespace TestOffice
{
    partial class AboutProgramForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutProgramForm));
            this.tbAboutProgram = new System.Windows.Forms.TextBox();
            this.cbDontShowAboutProgramForm = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // tbAboutProgram
            // 
            this.tbAboutProgram.Location = new System.Drawing.Point(9, 9);
            this.tbAboutProgram.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tbAboutProgram.Multiline = true;
            this.tbAboutProgram.Name = "tbAboutProgram";
            this.tbAboutProgram.ReadOnly = true;
            this.tbAboutProgram.Size = new System.Drawing.Size(426, 176);
            this.tbAboutProgram.TabIndex = 0;
            this.tbAboutProgram.TabStop = false;
            this.tbAboutProgram.Text = resources.GetString("tbAboutProgram.Text");
            // 
            // cbDontShowAboutProgramForm
            // 
            this.cbDontShowAboutProgramForm.AutoSize = true;
            this.cbDontShowAboutProgramForm.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbDontShowAboutProgramForm.Location = new System.Drawing.Point(9, 190);
            this.cbDontShowAboutProgramForm.Margin = new System.Windows.Forms.Padding(0);
            this.cbDontShowAboutProgramForm.Name = "cbDontShowAboutProgramForm";
            this.cbDontShowAboutProgramForm.Size = new System.Drawing.Size(354, 23);
            this.cbDontShowAboutProgramForm.TabIndex = 1;
            this.cbDontShowAboutProgramForm.Text = "Не показывать данное окно в дальнейшем";
            this.cbDontShowAboutProgramForm.UseVisualStyleBackColor = true;
            // 
            // AboutProgramForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 221);
            this.Controls.Add(this.cbDontShowAboutProgramForm);
            this.Controls.Add(this.tbAboutProgram);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutProgramForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About Program";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AboutProgramForm_FormClosing);
            this.Load += new System.EventHandler(this.AboutProgramForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbAboutProgram;
        private System.Windows.Forms.CheckBox cbDontShowAboutProgramForm;
    }
}