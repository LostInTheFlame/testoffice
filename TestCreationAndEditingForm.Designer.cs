﻿namespace TestOffice
{
    partial class TestCreationAndEditingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pGeneralSettings = new System.Windows.Forms.Panel();
            this.tbSetTime = new System.Windows.Forms.TextBox();
            this.cbShowCorrectOrIncorrectAnswer = new System.Windows.Forms.CheckBox();
            this.cbActivateTimer = new System.Windows.Forms.CheckBox();
            this.tbTopic = new System.Windows.Forms.TextBox();
            this.lTopic = new System.Windows.Forms.Label();
            this.pListQuestions = new System.Windows.Forms.Panel();
            this.lListQuestions = new System.Windows.Forms.Label();
            this.lbListQuestions = new System.Windows.Forms.ListBox();
            this.pQuestion = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDeleteQuestion = new System.Windows.Forms.Button();
            this.btnAddQuestion = new System.Windows.Forms.Button();
            this.btnEditQuestion = new System.Windows.Forms.Button();
            this.btnSaveTest = new System.Windows.Forms.Button();
            this.tbAnswer6 = new System.Windows.Forms.TextBox();
            this.tbAnswer5 = new System.Windows.Forms.TextBox();
            this.tbAnswer4 = new System.Windows.Forms.TextBox();
            this.tbAnswer3 = new System.Windows.Forms.TextBox();
            this.tbAnswer2 = new System.Windows.Forms.TextBox();
            this.tbAnswer1 = new System.Windows.Forms.TextBox();
            this.cbUserInputAnswer = new System.Windows.Forms.CheckBox();
            this.cbAnswer6 = new System.Windows.Forms.CheckBox();
            this.cbAnswer5 = new System.Windows.Forms.CheckBox();
            this.cbAnswer4 = new System.Windows.Forms.CheckBox();
            this.cbAnswer3 = new System.Windows.Forms.CheckBox();
            this.cbAnswer2 = new System.Windows.Forms.CheckBox();
            this.cbAnswer1 = new System.Windows.Forms.CheckBox();
            this.lAnswers = new System.Windows.Forms.Label();
            this.tbQuestion = new System.Windows.Forms.TextBox();
            this.lQuestion = new System.Windows.Forms.Label();
            this.sfdSaveTest = new System.Windows.Forms.SaveFileDialog();
            this.ofdOpenTest = new System.Windows.Forms.OpenFileDialog();
            this.pGeneralSettings.SuspendLayout();
            this.pListQuestions.SuspendLayout();
            this.pQuestion.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pGeneralSettings
            // 
            this.pGeneralSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pGeneralSettings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pGeneralSettings.Controls.Add(this.tbSetTime);
            this.pGeneralSettings.Controls.Add(this.cbShowCorrectOrIncorrectAnswer);
            this.pGeneralSettings.Controls.Add(this.cbActivateTimer);
            this.pGeneralSettings.Controls.Add(this.tbTopic);
            this.pGeneralSettings.Controls.Add(this.lTopic);
            this.pGeneralSettings.Location = new System.Drawing.Point(9, 9);
            this.pGeneralSettings.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.pGeneralSettings.Name = "pGeneralSettings";
            this.pGeneralSettings.Size = new System.Drawing.Size(766, 130);
            this.pGeneralSettings.TabIndex = 0;
            // 
            // tbSetTime
            // 
            this.tbSetTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSetTime.Enabled = false;
            this.tbSetTime.Location = new System.Drawing.Point(358, 73);
            this.tbSetTime.Margin = new System.Windows.Forms.Padding(5, 0, 10, 5);
            this.tbSetTime.Name = "tbSetTime";
            this.tbSetTime.Size = new System.Drawing.Size(396, 26);
            this.tbSetTime.TabIndex = 4;
            this.tbSetTime.TabStop = false;
            this.tbSetTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSetTime_KeyPress);
            // 
            // cbShowCorrectOrIncorrectAnswer
            // 
            this.cbShowCorrectOrIncorrectAnswer.AutoSize = true;
            this.cbShowCorrectOrIncorrectAnswer.Location = new System.Drawing.Point(10, 104);
            this.cbShowCorrectOrIncorrectAnswer.Margin = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.cbShowCorrectOrIncorrectAnswer.Name = "cbShowCorrectOrIncorrectAnswer";
            this.cbShowCorrectOrIncorrectAnswer.Size = new System.Drawing.Size(447, 22);
            this.cbShowCorrectOrIncorrectAnswer.TabIndex = 3;
            this.cbShowCorrectOrIncorrectAnswer.TabStop = false;
            this.cbShowCorrectOrIncorrectAnswer.Text = "Показывать правильные или неправильные ответы сразу";
            this.cbShowCorrectOrIncorrectAnswer.UseVisualStyleBackColor = true;
            // 
            // cbActivateTimer
            // 
            this.cbActivateTimer.AutoSize = true;
            this.cbActivateTimer.Location = new System.Drawing.Point(10, 75);
            this.cbActivateTimer.Margin = new System.Windows.Forms.Padding(10, 0, 0, 5);
            this.cbActivateTimer.Name = "cbActivateTimer";
            this.cbActivateTimer.Size = new System.Drawing.Size(343, 22);
            this.cbActivateTimer.TabIndex = 2;
            this.cbActivateTimer.TabStop = false;
            this.cbActivateTimer.Text = "Ограничить время выполнения теста (мин.)";
            this.cbActivateTimer.UseVisualStyleBackColor = true;
            this.cbActivateTimer.CheckedChanged += new System.EventHandler(this.cbActivateTimer_CheckedChanged);
            // 
            // tbTopic
            // 
            this.tbTopic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTopic.Location = new System.Drawing.Point(10, 28);
            this.tbTopic.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.tbTopic.Multiline = true;
            this.tbTopic.Name = "tbTopic";
            this.tbTopic.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbTopic.Size = new System.Drawing.Size(744, 40);
            this.tbTopic.TabIndex = 1;
            this.tbTopic.TabStop = false;
            // 
            // lTopic
            // 
            this.lTopic.AutoSize = true;
            this.lTopic.Location = new System.Drawing.Point(7, 5);
            this.lTopic.Margin = new System.Windows.Forms.Padding(6, 5, 5, 5);
            this.lTopic.Name = "lTopic";
            this.lTopic.Size = new System.Drawing.Size(49, 18);
            this.lTopic.TabIndex = 0;
            this.lTopic.Text = "Тема:";
            // 
            // pListQuestions
            // 
            this.pListQuestions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pListQuestions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pListQuestions.Controls.Add(this.lListQuestions);
            this.pListQuestions.Controls.Add(this.lbListQuestions);
            this.pListQuestions.Location = new System.Drawing.Point(9, 149);
            this.pListQuestions.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.pListQuestions.Name = "pListQuestions";
            this.pListQuestions.Size = new System.Drawing.Size(240, 403);
            this.pListQuestions.TabIndex = 1;
            // 
            // lListQuestions
            // 
            this.lListQuestions.AutoSize = true;
            this.lListQuestions.Location = new System.Drawing.Point(7, 5);
            this.lListQuestions.Margin = new System.Windows.Forms.Padding(5);
            this.lListQuestions.Name = "lListQuestions";
            this.lListQuestions.Size = new System.Drawing.Size(138, 18);
            this.lListQuestions.TabIndex = 18;
            this.lListQuestions.Text = "Список вопросов:";
            // 
            // lbListQuestions
            // 
            this.lbListQuestions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbListQuestions.FormattingEnabled = true;
            this.lbListQuestions.ItemHeight = 18;
            this.lbListQuestions.Location = new System.Drawing.Point(10, 28);
            this.lbListQuestions.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.lbListQuestions.Name = "lbListQuestions";
            this.lbListQuestions.Size = new System.Drawing.Size(218, 364);
            this.lbListQuestions.TabIndex = 0;
            this.lbListQuestions.TabStop = false;
            this.lbListQuestions.SelectedIndexChanged += new System.EventHandler(this.lbListQuestions_SelectedIndexChanged);
            // 
            // pQuestion
            // 
            this.pQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pQuestion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pQuestion.Controls.Add(this.tableLayoutPanel1);
            this.pQuestion.Controls.Add(this.tbAnswer6);
            this.pQuestion.Controls.Add(this.tbAnswer5);
            this.pQuestion.Controls.Add(this.tbAnswer4);
            this.pQuestion.Controls.Add(this.tbAnswer3);
            this.pQuestion.Controls.Add(this.tbAnswer2);
            this.pQuestion.Controls.Add(this.tbAnswer1);
            this.pQuestion.Controls.Add(this.cbUserInputAnswer);
            this.pQuestion.Controls.Add(this.cbAnswer6);
            this.pQuestion.Controls.Add(this.cbAnswer5);
            this.pQuestion.Controls.Add(this.cbAnswer4);
            this.pQuestion.Controls.Add(this.cbAnswer3);
            this.pQuestion.Controls.Add(this.cbAnswer2);
            this.pQuestion.Controls.Add(this.cbAnswer1);
            this.pQuestion.Controls.Add(this.lAnswers);
            this.pQuestion.Controls.Add(this.tbQuestion);
            this.pQuestion.Controls.Add(this.lQuestion);
            this.pQuestion.Location = new System.Drawing.Point(259, 149);
            this.pQuestion.Margin = new System.Windows.Forms.Padding(0);
            this.pQuestion.Name = "pQuestion";
            this.pQuestion.Size = new System.Drawing.Size(516, 403);
            this.pQuestion.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnDeleteQuestion, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnAddQuestion, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnEditQuestion, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSaveTest, 4, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 351);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(494, 40);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // btnDeleteQuestion
            // 
            this.btnDeleteQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnDeleteQuestion.Enabled = false;
            this.btnDeleteQuestion.Location = new System.Drawing.Point(198, 0);
            this.btnDeleteQuestion.Margin = new System.Windows.Forms.Padding(0);
            this.btnDeleteQuestion.Name = "btnDeleteQuestion";
            this.btnDeleteQuestion.Size = new System.Drawing.Size(94, 40);
            this.btnDeleteQuestion.TabIndex = 2;
            this.btnDeleteQuestion.TabStop = false;
            this.btnDeleteQuestion.Text = "Удалить";
            this.btnDeleteQuestion.UseVisualStyleBackColor = true;
            this.btnDeleteQuestion.Click += new System.EventHandler(this.btnDeleteQuestion_Click);
            // 
            // btnAddQuestion
            // 
            this.btnAddQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnAddQuestion.Location = new System.Drawing.Point(2, 0);
            this.btnAddQuestion.Margin = new System.Windows.Forms.Padding(0);
            this.btnAddQuestion.Name = "btnAddQuestion";
            this.btnAddQuestion.Size = new System.Drawing.Size(94, 40);
            this.btnAddQuestion.TabIndex = 0;
            this.btnAddQuestion.TabStop = false;
            this.btnAddQuestion.Text = "Добавить";
            this.btnAddQuestion.UseVisualStyleBackColor = true;
            this.btnAddQuestion.Click += new System.EventHandler(this.btnAddQuestion_Click);
            // 
            // btnEditQuestion
            // 
            this.btnEditQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnEditQuestion.Enabled = false;
            this.btnEditQuestion.Location = new System.Drawing.Point(100, 0);
            this.btnEditQuestion.Margin = new System.Windows.Forms.Padding(0);
            this.btnEditQuestion.Name = "btnEditQuestion";
            this.btnEditQuestion.Size = new System.Drawing.Size(94, 40);
            this.btnEditQuestion.TabIndex = 1;
            this.btnEditQuestion.TabStop = false;
            this.btnEditQuestion.Text = "Изменить";
            this.btnEditQuestion.UseVisualStyleBackColor = true;
            this.btnEditQuestion.Click += new System.EventHandler(this.btnEditQuestion_Click);
            // 
            // btnSaveTest
            // 
            this.btnSaveTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnSaveTest.Location = new System.Drawing.Point(396, 0);
            this.btnSaveTest.Margin = new System.Windows.Forms.Padding(0);
            this.btnSaveTest.Name = "btnSaveTest";
            this.btnSaveTest.Size = new System.Drawing.Size(94, 40);
            this.btnSaveTest.TabIndex = 3;
            this.btnSaveTest.TabStop = false;
            this.btnSaveTest.Text = "Сохранить";
            this.btnSaveTest.UseVisualStyleBackColor = true;
            this.btnSaveTest.Click += new System.EventHandler(this.btnSaveTest_Click);
            // 
            // tbAnswer6
            // 
            this.tbAnswer6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAnswer6.Location = new System.Drawing.Point(55, 266);
            this.tbAnswer6.Margin = new System.Windows.Forms.Padding(0, 0, 10, 5);
            this.tbAnswer6.Name = "tbAnswer6";
            this.tbAnswer6.Size = new System.Drawing.Size(449, 26);
            this.tbAnswer6.TabIndex = 17;
            this.tbAnswer6.TabStop = false;
            this.tbAnswer6.Tag = "TextAnswer";
            // 
            // tbAnswer5
            // 
            this.tbAnswer5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAnswer5.Location = new System.Drawing.Point(55, 235);
            this.tbAnswer5.Margin = new System.Windows.Forms.Padding(0, 0, 10, 5);
            this.tbAnswer5.Name = "tbAnswer5";
            this.tbAnswer5.Size = new System.Drawing.Size(449, 26);
            this.tbAnswer5.TabIndex = 16;
            this.tbAnswer5.TabStop = false;
            this.tbAnswer5.Tag = "TextAnswer";
            // 
            // tbAnswer4
            // 
            this.tbAnswer4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAnswer4.Location = new System.Drawing.Point(55, 204);
            this.tbAnswer4.Margin = new System.Windows.Forms.Padding(0, 0, 10, 5);
            this.tbAnswer4.Name = "tbAnswer4";
            this.tbAnswer4.Size = new System.Drawing.Size(449, 26);
            this.tbAnswer4.TabIndex = 15;
            this.tbAnswer4.TabStop = false;
            this.tbAnswer4.Tag = "TextAnswer";
            // 
            // tbAnswer3
            // 
            this.tbAnswer3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAnswer3.Location = new System.Drawing.Point(55, 173);
            this.tbAnswer3.Margin = new System.Windows.Forms.Padding(0, 0, 10, 5);
            this.tbAnswer3.Name = "tbAnswer3";
            this.tbAnswer3.Size = new System.Drawing.Size(449, 26);
            this.tbAnswer3.TabIndex = 14;
            this.tbAnswer3.TabStop = false;
            this.tbAnswer3.Tag = "TextAnswer";
            // 
            // tbAnswer2
            // 
            this.tbAnswer2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAnswer2.Location = new System.Drawing.Point(55, 142);
            this.tbAnswer2.Margin = new System.Windows.Forms.Padding(0, 0, 10, 5);
            this.tbAnswer2.Name = "tbAnswer2";
            this.tbAnswer2.Size = new System.Drawing.Size(449, 26);
            this.tbAnswer2.TabIndex = 13;
            this.tbAnswer2.TabStop = false;
            this.tbAnswer2.Tag = "TextAnswer";
            // 
            // tbAnswer1
            // 
            this.tbAnswer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAnswer1.Location = new System.Drawing.Point(55, 111);
            this.tbAnswer1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 5);
            this.tbAnswer1.Name = "tbAnswer1";
            this.tbAnswer1.Size = new System.Drawing.Size(449, 26);
            this.tbAnswer1.TabIndex = 12;
            this.tbAnswer1.TabStop = false;
            this.tbAnswer1.Tag = "TextAnswer";
            // 
            // cbUserInputAnswer
            // 
            this.cbUserInputAnswer.AutoSize = true;
            this.cbUserInputAnswer.Location = new System.Drawing.Point(10, 297);
            this.cbUserInputAnswer.Margin = new System.Windows.Forms.Padding(10, 0, 5, 10);
            this.cbUserInputAnswer.Name = "cbUserInputAnswer";
            this.cbUserInputAnswer.Size = new System.Drawing.Size(384, 22);
            this.cbUserInputAnswer.TabIndex = 11;
            this.cbUserInputAnswer.TabStop = false;
            this.cbUserInputAnswer.Text = "Дать возможность самостоятельно ввести ответ";
            this.cbUserInputAnswer.UseVisualStyleBackColor = true;
            this.cbUserInputAnswer.CheckedChanged += new System.EventHandler(this.cbUserInputAnswer_CheckedChanged);
            // 
            // cbAnswer6
            // 
            this.cbAnswer6.AutoSize = true;
            this.cbAnswer6.Location = new System.Drawing.Point(10, 268);
            this.cbAnswer6.Margin = new System.Windows.Forms.Padding(10, 0, 5, 5);
            this.cbAnswer6.Name = "cbAnswer6";
            this.cbAnswer6.Size = new System.Drawing.Size(40, 22);
            this.cbAnswer6.TabIndex = 10;
            this.cbAnswer6.TabStop = false;
            this.cbAnswer6.Tag = "CheckedAnswer";
            this.cbAnswer6.Text = "6.";
            this.cbAnswer6.UseVisualStyleBackColor = true;
            // 
            // cbAnswer5
            // 
            this.cbAnswer5.AutoSize = true;
            this.cbAnswer5.Location = new System.Drawing.Point(10, 237);
            this.cbAnswer5.Margin = new System.Windows.Forms.Padding(10, 0, 5, 5);
            this.cbAnswer5.Name = "cbAnswer5";
            this.cbAnswer5.Size = new System.Drawing.Size(40, 22);
            this.cbAnswer5.TabIndex = 9;
            this.cbAnswer5.TabStop = false;
            this.cbAnswer5.Tag = "CheckedAnswer";
            this.cbAnswer5.Text = "5.";
            this.cbAnswer5.UseVisualStyleBackColor = true;
            // 
            // cbAnswer4
            // 
            this.cbAnswer4.AutoSize = true;
            this.cbAnswer4.Location = new System.Drawing.Point(10, 206);
            this.cbAnswer4.Margin = new System.Windows.Forms.Padding(10, 0, 5, 5);
            this.cbAnswer4.Name = "cbAnswer4";
            this.cbAnswer4.Size = new System.Drawing.Size(40, 22);
            this.cbAnswer4.TabIndex = 8;
            this.cbAnswer4.TabStop = false;
            this.cbAnswer4.Tag = "CheckedAnswer";
            this.cbAnswer4.Text = "4.";
            this.cbAnswer4.UseVisualStyleBackColor = true;
            // 
            // cbAnswer3
            // 
            this.cbAnswer3.AutoSize = true;
            this.cbAnswer3.Location = new System.Drawing.Point(10, 175);
            this.cbAnswer3.Margin = new System.Windows.Forms.Padding(10, 0, 5, 5);
            this.cbAnswer3.Name = "cbAnswer3";
            this.cbAnswer3.Size = new System.Drawing.Size(40, 22);
            this.cbAnswer3.TabIndex = 7;
            this.cbAnswer3.TabStop = false;
            this.cbAnswer3.Tag = "CheckedAnswer";
            this.cbAnswer3.Text = "3.";
            this.cbAnswer3.UseVisualStyleBackColor = true;
            // 
            // cbAnswer2
            // 
            this.cbAnswer2.AutoSize = true;
            this.cbAnswer2.Location = new System.Drawing.Point(10, 144);
            this.cbAnswer2.Margin = new System.Windows.Forms.Padding(10, 0, 5, 5);
            this.cbAnswer2.Name = "cbAnswer2";
            this.cbAnswer2.Size = new System.Drawing.Size(40, 22);
            this.cbAnswer2.TabIndex = 6;
            this.cbAnswer2.TabStop = false;
            this.cbAnswer2.Tag = "CheckedAnswer";
            this.cbAnswer2.Text = "2.";
            this.cbAnswer2.UseVisualStyleBackColor = true;
            // 
            // cbAnswer1
            // 
            this.cbAnswer1.AutoSize = true;
            this.cbAnswer1.Location = new System.Drawing.Point(10, 113);
            this.cbAnswer1.Margin = new System.Windows.Forms.Padding(10, 0, 5, 5);
            this.cbAnswer1.Name = "cbAnswer1";
            this.cbAnswer1.Size = new System.Drawing.Size(40, 22);
            this.cbAnswer1.TabIndex = 5;
            this.cbAnswer1.TabStop = false;
            this.cbAnswer1.Tag = "CheckedAnswer";
            this.cbAnswer1.Text = "1.";
            this.cbAnswer1.UseVisualStyleBackColor = true;
            // 
            // lAnswers
            // 
            this.lAnswers.AutoSize = true;
            this.lAnswers.Location = new System.Drawing.Point(7, 88);
            this.lAnswers.Margin = new System.Windows.Forms.Padding(10, 0, 5, 5);
            this.lAnswers.Name = "lAnswers";
            this.lAnswers.Size = new System.Drawing.Size(147, 18);
            this.lAnswers.TabIndex = 4;
            this.lAnswers.Text = "Варианты ответов:";
            // 
            // tbQuestion
            // 
            this.tbQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbQuestion.Location = new System.Drawing.Point(10, 28);
            this.tbQuestion.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.tbQuestion.Multiline = true;
            this.tbQuestion.Name = "tbQuestion";
            this.tbQuestion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbQuestion.Size = new System.Drawing.Size(494, 50);
            this.tbQuestion.TabIndex = 3;
            this.tbQuestion.TabStop = false;
            // 
            // lQuestion
            // 
            this.lQuestion.AutoSize = true;
            this.lQuestion.Location = new System.Drawing.Point(7, 5);
            this.lQuestion.Margin = new System.Windows.Forms.Padding(6, 5, 5, 5);
            this.lQuestion.Name = "lQuestion";
            this.lQuestion.Size = new System.Drawing.Size(66, 18);
            this.lQuestion.TabIndex = 2;
            this.lQuestion.Text = "Вопрос:";
            // 
            // ofdOpenTest
            // 
            this.ofdOpenTest.FileName = "openFileDialog1";
            // 
            // TestCreationAndEditingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pQuestion);
            this.Controls.Add(this.pListQuestions);
            this.Controls.Add(this.pGeneralSettings);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TestCreationAndEditingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TestOffice";
            this.pGeneralSettings.ResumeLayout(false);
            this.pGeneralSettings.PerformLayout();
            this.pListQuestions.ResumeLayout(false);
            this.pListQuestions.PerformLayout();
            this.pQuestion.ResumeLayout(false);
            this.pQuestion.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pGeneralSettings;
        private System.Windows.Forms.CheckBox cbShowCorrectOrIncorrectAnswer;
        private System.Windows.Forms.CheckBox cbActivateTimer;
        private System.Windows.Forms.TextBox tbTopic;
        private System.Windows.Forms.Label lTopic;
        private System.Windows.Forms.TextBox tbSetTime;
        private System.Windows.Forms.Panel pListQuestions;
        private System.Windows.Forms.Panel pQuestion;
        private System.Windows.Forms.TextBox tbQuestion;
        private System.Windows.Forms.Label lQuestion;
        private System.Windows.Forms.CheckBox cbUserInputAnswer;
        private System.Windows.Forms.CheckBox cbAnswer6;
        private System.Windows.Forms.CheckBox cbAnswer5;
        private System.Windows.Forms.CheckBox cbAnswer4;
        private System.Windows.Forms.CheckBox cbAnswer3;
        private System.Windows.Forms.CheckBox cbAnswer2;
        private System.Windows.Forms.CheckBox cbAnswer1;
        private System.Windows.Forms.Label lAnswers;
        private System.Windows.Forms.TextBox tbAnswer6;
        private System.Windows.Forms.TextBox tbAnswer5;
        private System.Windows.Forms.TextBox tbAnswer4;
        private System.Windows.Forms.TextBox tbAnswer3;
        private System.Windows.Forms.TextBox tbAnswer2;
        private System.Windows.Forms.TextBox tbAnswer1;
        private System.Windows.Forms.ListBox lbListQuestions;
        private System.Windows.Forms.Label lListQuestions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnAddQuestion;
        private System.Windows.Forms.Button btnEditQuestion;
        private System.Windows.Forms.Button btnDeleteQuestion;
        private System.Windows.Forms.Button btnSaveTest;
        private System.Windows.Forms.SaveFileDialog sfdSaveTest;
        private System.Windows.Forms.OpenFileDialog ofdOpenTest;
    }
}