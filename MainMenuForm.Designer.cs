﻿namespace TestOffice
{
    partial class MainMenuForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.msTopMenu = new System.Windows.Forms.MenuStrip();
            this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCreateNewTest = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEditTest = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiTakeTest = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAboutProgram = new System.Windows.Forms.ToolStripMenuItem();
            this.pDuplicateMenuFile = new System.Windows.Forms.Panel();
            this.btnEditTest = new System.Windows.Forms.Button();
            this.btnTakeTest = new System.Windows.Forms.Button();
            this.btnCreateNewTest = new System.Windows.Forms.Button();
            this.pTestResults = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnViewTestResults = new System.Windows.Forms.Button();
            this.btnDeleteTestResults = new System.Windows.Forms.Button();
            this.lTestResults = new System.Windows.Forms.Label();
            this.lbListUserTestResults = new System.Windows.Forms.ListBox();
            this.msTopMenu.SuspendLayout();
            this.pDuplicateMenuFile.SuspendLayout();
            this.pTestResults.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // msTopMenu
            // 
            this.msTopMenu.BackColor = System.Drawing.SystemColors.Control;
            this.msTopMenu.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.msTopMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile,
            this.tsmiAboutProgram});
            this.msTopMenu.Location = new System.Drawing.Point(0, 0);
            this.msTopMenu.Name = "msTopMenu";
            this.msTopMenu.Size = new System.Drawing.Size(784, 26);
            this.msTopMenu.TabIndex = 0;
            this.msTopMenu.Text = "TopMenu";
            // 
            // tsmiFile
            // 
            this.tsmiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCreateNewTest,
            this.tsmiEditTest,
            this.toolStripMenuItem1,
            this.tsmiTakeTest});
            this.tsmiFile.Name = "tsmiFile";
            this.tsmiFile.Size = new System.Drawing.Size(56, 22);
            this.tsmiFile.Text = "Файл";
            // 
            // tsmiCreateNewTest
            // 
            this.tsmiCreateNewTest.Name = "tsmiCreateNewTest";
            this.tsmiCreateNewTest.Size = new System.Drawing.Size(223, 22);
            this.tsmiCreateNewTest.Text = "Создать новый тест";
            this.tsmiCreateNewTest.Click += new System.EventHandler(this.tsmiCreateNewTest_Click);
            // 
            // tsmiEditTest
            // 
            this.tsmiEditTest.Name = "tsmiEditTest";
            this.tsmiEditTest.Size = new System.Drawing.Size(223, 22);
            this.tsmiEditTest.Text = "Редактировать тест";
            this.tsmiEditTest.Click += new System.EventHandler(this.tsmiEditTest_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(220, 6);
            // 
            // tsmiTakeTest
            // 
            this.tsmiTakeTest.Name = "tsmiTakeTest";
            this.tsmiTakeTest.Size = new System.Drawing.Size(223, 22);
            this.tsmiTakeTest.Text = "Пройти тест";
            this.tsmiTakeTest.Click += new System.EventHandler(this.tsmiTakeTest_Click);
            // 
            // tsmiAboutProgram
            // 
            this.tsmiAboutProgram.Name = "tsmiAboutProgram";
            this.tsmiAboutProgram.Size = new System.Drawing.Size(117, 22);
            this.tsmiAboutProgram.Text = "О программе";
            this.tsmiAboutProgram.Click += new System.EventHandler(this.tsmiAboutProgram_Click);
            // 
            // pDuplicateMenuFile
            // 
            this.pDuplicateMenuFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pDuplicateMenuFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pDuplicateMenuFile.Controls.Add(this.btnEditTest);
            this.pDuplicateMenuFile.Controls.Add(this.btnTakeTest);
            this.pDuplicateMenuFile.Controls.Add(this.btnCreateNewTest);
            this.pDuplicateMenuFile.Location = new System.Drawing.Point(9, 36);
            this.pDuplicateMenuFile.Margin = new System.Windows.Forms.Padding(0, 10, 10, 0);
            this.pDuplicateMenuFile.Name = "pDuplicateMenuFile";
            this.pDuplicateMenuFile.Size = new System.Drawing.Size(150, 516);
            this.pDuplicateMenuFile.TabIndex = 1;
            // 
            // btnEditTest
            // 
            this.btnEditTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditTest.Location = new System.Drawing.Point(10, 70);
            this.btnEditTest.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.btnEditTest.Name = "btnEditTest";
            this.btnEditTest.Size = new System.Drawing.Size(130, 50);
            this.btnEditTest.TabIndex = 2;
            this.btnEditTest.TabStop = false;
            this.btnEditTest.Text = "Редактировать тест";
            this.btnEditTest.UseVisualStyleBackColor = true;
            this.btnEditTest.Click += new System.EventHandler(this.btnEditTest_Click);
            // 
            // btnTakeTest
            // 
            this.btnTakeTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTakeTest.Location = new System.Drawing.Point(10, 130);
            this.btnTakeTest.Margin = new System.Windows.Forms.Padding(0);
            this.btnTakeTest.Name = "btnTakeTest";
            this.btnTakeTest.Size = new System.Drawing.Size(130, 50);
            this.btnTakeTest.TabIndex = 1;
            this.btnTakeTest.TabStop = false;
            this.btnTakeTest.Text = "Пройти тест";
            this.btnTakeTest.UseVisualStyleBackColor = true;
            this.btnTakeTest.Click += new System.EventHandler(this.btnTakeTest_Click);
            // 
            // btnCreateNewTest
            // 
            this.btnCreateNewTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateNewTest.Location = new System.Drawing.Point(10, 10);
            this.btnCreateNewTest.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.btnCreateNewTest.Name = "btnCreateNewTest";
            this.btnCreateNewTest.Size = new System.Drawing.Size(130, 50);
            this.btnCreateNewTest.TabIndex = 0;
            this.btnCreateNewTest.TabStop = false;
            this.btnCreateNewTest.Text = "Создать тест";
            this.btnCreateNewTest.UseVisualStyleBackColor = true;
            this.btnCreateNewTest.Click += new System.EventHandler(this.btnCreateNewTest_Click);
            // 
            // pTestResults
            // 
            this.pTestResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pTestResults.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTestResults.Controls.Add(this.tableLayoutPanel1);
            this.pTestResults.Controls.Add(this.lTestResults);
            this.pTestResults.Controls.Add(this.lbListUserTestResults);
            this.pTestResults.Location = new System.Drawing.Point(169, 36);
            this.pTestResults.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.pTestResults.Name = "pTestResults";
            this.pTestResults.Size = new System.Drawing.Size(606, 516);
            this.pTestResults.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btnViewTestResults, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnDeleteTestResults, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 454);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(581, 50);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // btnViewTestResults
            // 
            this.btnViewTestResults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnViewTestResults.Location = new System.Drawing.Point(80, 0);
            this.btnViewTestResults.Margin = new System.Windows.Forms.Padding(0);
            this.btnViewTestResults.Name = "btnViewTestResults";
            this.btnViewTestResults.Size = new System.Drawing.Size(130, 50);
            this.btnViewTestResults.TabIndex = 0;
            this.btnViewTestResults.TabStop = false;
            this.btnViewTestResults.Text = "Посмотреть";
            this.btnViewTestResults.UseVisualStyleBackColor = true;
            this.btnViewTestResults.Click += new System.EventHandler(this.btnViewTestResults_Click);
            // 
            // btnDeleteTestResults
            // 
            this.btnDeleteTestResults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnDeleteTestResults.Location = new System.Drawing.Point(370, 0);
            this.btnDeleteTestResults.Margin = new System.Windows.Forms.Padding(0);
            this.btnDeleteTestResults.Name = "btnDeleteTestResults";
            this.btnDeleteTestResults.Size = new System.Drawing.Size(130, 50);
            this.btnDeleteTestResults.TabIndex = 1;
            this.btnDeleteTestResults.TabStop = false;
            this.btnDeleteTestResults.Text = "Удалить";
            this.btnDeleteTestResults.UseVisualStyleBackColor = true;
            this.btnDeleteTestResults.Click += new System.EventHandler(this.btnDeleteTestResults_Click);
            // 
            // lTestResults
            // 
            this.lTestResults.AutoSize = true;
            this.lTestResults.Location = new System.Drawing.Point(7, 10);
            this.lTestResults.Margin = new System.Windows.Forms.Padding(10);
            this.lTestResults.Name = "lTestResults";
            this.lTestResults.Size = new System.Drawing.Size(312, 18);
            this.lTestResults.TabIndex = 1;
            this.lTestResults.Text = "Результаты тестирования пользователей:";
            // 
            // lbListUserTestResults
            // 
            this.lbListUserTestResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbListUserTestResults.FormattingEnabled = true;
            this.lbListUserTestResults.ItemHeight = 18;
            this.lbListUserTestResults.Location = new System.Drawing.Point(10, 38);
            this.lbListUserTestResults.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.lbListUserTestResults.Name = "lbListUserTestResults";
            this.lbListUserTestResults.Size = new System.Drawing.Size(584, 400);
            this.lbListUserTestResults.TabIndex = 0;
            this.lbListUserTestResults.TabStop = false;
            this.lbListUserTestResults.SelectedIndexChanged += new System.EventHandler(this.lbListUserTestResults_SelectedIndexChanged);
            // 
            // MainMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pTestResults);
            this.Controls.Add(this.pDuplicateMenuFile);
            this.Controls.Add(this.msTopMenu);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.msTopMenu;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainMenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TestOffice";
            this.Activated += new System.EventHandler(this.MainMenuForm_Activated);
            this.Load += new System.EventHandler(this.MainMenuForm_Load);
            this.msTopMenu.ResumeLayout(false);
            this.msTopMenu.PerformLayout();
            this.pDuplicateMenuFile.ResumeLayout(false);
            this.pTestResults.ResumeLayout(false);
            this.pTestResults.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msTopMenu;
        private System.Windows.Forms.ToolStripMenuItem tsmiFile;
        private System.Windows.Forms.ToolStripMenuItem tsmiCreateNewTest;
        private System.Windows.Forms.ToolStripMenuItem tsmiEditTest;
        private System.Windows.Forms.ToolStripMenuItem tsmiTakeTest;
        private System.Windows.Forms.ToolStripMenuItem tsmiAboutProgram;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.Panel pDuplicateMenuFile;
        private System.Windows.Forms.Button btnEditTest;
        private System.Windows.Forms.Button btnTakeTest;
        private System.Windows.Forms.Button btnCreateNewTest;
        private System.Windows.Forms.Panel pTestResults;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnViewTestResults;
        private System.Windows.Forms.Button btnDeleteTestResults;
        private System.Windows.Forms.Label lTestResults;
        private System.Windows.Forms.ListBox lbListUserTestResults;
    }
}

