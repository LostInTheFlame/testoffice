﻿using System;
using System.Windows.Forms;

namespace TestOffice
{
    public partial class AboutProgramForm : Form
    {
        public AboutProgramForm()
        {
            InitializeComponent();
        }

        private void AboutProgramForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.DontShowAboutProgramForm = cbDontShowAboutProgramForm.Checked;
            Properties.Settings.Default.Save();
        }

        private void AboutProgramForm_Load(object sender, EventArgs e)
        {
            cbDontShowAboutProgramForm.Checked = Properties.Settings.Default.DontShowAboutProgramForm;
        }
    }
}
