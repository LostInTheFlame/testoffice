﻿namespace TestOffice
{
    partial class TestResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpStatistics = new System.Windows.Forms.TableLayoutPanel();
            this.lNumberOfCorrectAnswers = new System.Windows.Forms.Label();
            this.lNumberOfIncorrectAnswers = new System.Windows.Forms.Label();
            this.lUnansweredQuestions = new System.Windows.Forms.Label();
            this.pTestInformation = new System.Windows.Forms.Panel();
            this.btnNextQuestion = new System.Windows.Forms.Button();
            this.btnPreviousQuestion = new System.Windows.Forms.Button();
            this.tcAnswers = new System.Windows.Forms.TabControl();
            this.tpUserAnswers = new System.Windows.Forms.TabPage();
            this.cbAnswer6 = new System.Windows.Forms.CheckBox();
            this.cbAnswer5 = new System.Windows.Forms.CheckBox();
            this.cbAnswer4 = new System.Windows.Forms.CheckBox();
            this.cbAnswer3 = new System.Windows.Forms.CheckBox();
            this.cbAnswer2 = new System.Windows.Forms.CheckBox();
            this.cbAnswer1 = new System.Windows.Forms.CheckBox();
            this.tbUserInputAnswer = new System.Windows.Forms.TextBox();
            this.lUserInputAnswer = new System.Windows.Forms.Label();
            this.tpCorrectAnswers = new System.Windows.Forms.TabPage();
            this.lInputAnswer = new System.Windows.Forms.Label();
            this.cbCorrectAnswer6 = new System.Windows.Forms.CheckBox();
            this.cbCorrectAnswer5 = new System.Windows.Forms.CheckBox();
            this.cbCorrectAnswer4 = new System.Windows.Forms.CheckBox();
            this.cbCorrectAnswer3 = new System.Windows.Forms.CheckBox();
            this.cbCorrectAnswer2 = new System.Windows.Forms.CheckBox();
            this.cbCorrectAnswer1 = new System.Windows.Forms.CheckBox();
            this.tbQuestion = new System.Windows.Forms.TextBox();
            this.tbTopic = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lQuestionNumber = new System.Windows.Forms.Label();
            this.lCorrectAnswer = new System.Windows.Forms.Label();
            this.tlpStatistics.SuspendLayout();
            this.pTestInformation.SuspendLayout();
            this.tcAnswers.SuspendLayout();
            this.tpUserAnswers.SuspendLayout();
            this.tpCorrectAnswers.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpStatistics
            // 
            this.tlpStatistics.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpStatistics.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tlpStatistics.ColumnCount = 3;
            this.tlpStatistics.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tlpStatistics.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tlpStatistics.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tlpStatistics.Controls.Add(this.lNumberOfCorrectAnswers, 0, 0);
            this.tlpStatistics.Controls.Add(this.lNumberOfIncorrectAnswers, 1, 0);
            this.tlpStatistics.Controls.Add(this.lUnansweredQuestions, 2, 0);
            this.tlpStatistics.Location = new System.Drawing.Point(9, 9);
            this.tlpStatistics.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.tlpStatistics.Name = "tlpStatistics";
            this.tlpStatistics.RowCount = 1;
            this.tlpStatistics.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpStatistics.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tlpStatistics.Size = new System.Drawing.Size(766, 25);
            this.tlpStatistics.TabIndex = 0;
            // 
            // lNumberOfCorrectAnswers
            // 
            this.lNumberOfCorrectAnswers.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lNumberOfCorrectAnswers.AutoSize = true;
            this.lNumberOfCorrectAnswers.Location = new System.Drawing.Point(4, 3);
            this.lNumberOfCorrectAnswers.Name = "lNumberOfCorrectAnswers";
            this.lNumberOfCorrectAnswers.Size = new System.Drawing.Size(164, 18);
            this.lNumberOfCorrectAnswers.TabIndex = 0;
            this.lNumberOfCorrectAnswers.Text = "Правильных ответов:";
            // 
            // lNumberOfIncorrectAnswers
            // 
            this.lNumberOfIncorrectAnswers.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lNumberOfIncorrectAnswers.AutoSize = true;
            this.lNumberOfIncorrectAnswers.Location = new System.Drawing.Point(256, 3);
            this.lNumberOfIncorrectAnswers.Name = "lNumberOfIncorrectAnswers";
            this.lNumberOfIncorrectAnswers.Size = new System.Drawing.Size(181, 18);
            this.lNumberOfIncorrectAnswers.TabIndex = 1;
            this.lNumberOfIncorrectAnswers.Text = "Неправильных ответов:";
            // 
            // lUnansweredQuestions
            // 
            this.lUnansweredQuestions.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lUnansweredQuestions.AutoSize = true;
            this.lUnansweredQuestions.Location = new System.Drawing.Point(508, 3);
            this.lUnansweredQuestions.Name = "lUnansweredQuestions";
            this.lUnansweredQuestions.Size = new System.Drawing.Size(191, 18);
            this.lUnansweredQuestions.TabIndex = 2;
            this.lUnansweredQuestions.Text = "Неотвеченных вопросов:";
            // 
            // pTestInformation
            // 
            this.pTestInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pTestInformation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTestInformation.Controls.Add(this.btnNextQuestion);
            this.pTestInformation.Controls.Add(this.btnPreviousQuestion);
            this.pTestInformation.Controls.Add(this.tcAnswers);
            this.pTestInformation.Controls.Add(this.tbQuestion);
            this.pTestInformation.Controls.Add(this.tbTopic);
            this.pTestInformation.Location = new System.Drawing.Point(9, 44);
            this.pTestInformation.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.pTestInformation.Name = "pTestInformation";
            this.pTestInformation.Size = new System.Drawing.Size(766, 473);
            this.pTestInformation.TabIndex = 1;
            // 
            // btnNextQuestion
            // 
            this.btnNextQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextQuestion.Location = new System.Drawing.Point(634, 421);
            this.btnNextQuestion.Margin = new System.Windows.Forms.Padding(0, 0, 10, 10);
            this.btnNextQuestion.Name = "btnNextQuestion";
            this.btnNextQuestion.Size = new System.Drawing.Size(120, 40);
            this.btnNextQuestion.TabIndex = 4;
            this.btnNextQuestion.TabStop = false;
            this.btnNextQuestion.Text = "Следующий";
            this.btnNextQuestion.UseVisualStyleBackColor = true;
            this.btnNextQuestion.Click += new System.EventHandler(this.btnNextQuestion_Click);
            // 
            // btnPreviousQuestion
            // 
            this.btnPreviousQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreviousQuestion.Location = new System.Drawing.Point(10, 421);
            this.btnPreviousQuestion.Margin = new System.Windows.Forms.Padding(10, 0, 0, 10);
            this.btnPreviousQuestion.Name = "btnPreviousQuestion";
            this.btnPreviousQuestion.Size = new System.Drawing.Size(120, 40);
            this.btnPreviousQuestion.TabIndex = 3;
            this.btnPreviousQuestion.TabStop = false;
            this.btnPreviousQuestion.Text = "Предыдущий";
            this.btnPreviousQuestion.UseVisualStyleBackColor = true;
            this.btnPreviousQuestion.Click += new System.EventHandler(this.btnPreviousQuestion_Click);
            // 
            // tcAnswers
            // 
            this.tcAnswers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcAnswers.Controls.Add(this.tpUserAnswers);
            this.tcAnswers.Controls.Add(this.tpCorrectAnswers);
            this.tcAnswers.Location = new System.Drawing.Point(10, 130);
            this.tcAnswers.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.tcAnswers.Name = "tcAnswers";
            this.tcAnswers.SelectedIndex = 0;
            this.tcAnswers.Size = new System.Drawing.Size(744, 281);
            this.tcAnswers.TabIndex = 2;
            this.tcAnswers.TabStop = false;
            // 
            // tpUserAnswers
            // 
            this.tpUserAnswers.Controls.Add(this.cbAnswer6);
            this.tpUserAnswers.Controls.Add(this.cbAnswer5);
            this.tpUserAnswers.Controls.Add(this.cbAnswer4);
            this.tpUserAnswers.Controls.Add(this.cbAnswer3);
            this.tpUserAnswers.Controls.Add(this.cbAnswer2);
            this.tpUserAnswers.Controls.Add(this.cbAnswer1);
            this.tpUserAnswers.Controls.Add(this.tbUserInputAnswer);
            this.tpUserAnswers.Controls.Add(this.lUserInputAnswer);
            this.tpUserAnswers.Location = new System.Drawing.Point(4, 27);
            this.tpUserAnswers.Name = "tpUserAnswers";
            this.tpUserAnswers.Padding = new System.Windows.Forms.Padding(3);
            this.tpUserAnswers.Size = new System.Drawing.Size(736, 250);
            this.tpUserAnswers.TabIndex = 0;
            this.tpUserAnswers.Text = "Ваши ответы";
            this.tpUserAnswers.UseVisualStyleBackColor = true;
            // 
            // cbAnswer6
            // 
            this.cbAnswer6.AutoSize = true;
            this.cbAnswer6.Enabled = false;
            this.cbAnswer6.Location = new System.Drawing.Point(13, 184);
            this.cbAnswer6.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbAnswer6.Name = "cbAnswer6";
            this.cbAnswer6.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer6.TabIndex = 7;
            this.cbAnswer6.TabStop = false;
            this.cbAnswer6.Text = "Ответ 6";
            this.cbAnswer6.UseVisualStyleBackColor = true;
            // 
            // cbAnswer5
            // 
            this.cbAnswer5.AutoSize = true;
            this.cbAnswer5.Enabled = false;
            this.cbAnswer5.Location = new System.Drawing.Point(13, 157);
            this.cbAnswer5.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbAnswer5.Name = "cbAnswer5";
            this.cbAnswer5.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer5.TabIndex = 6;
            this.cbAnswer5.TabStop = false;
            this.cbAnswer5.Text = "Ответ 5";
            this.cbAnswer5.UseVisualStyleBackColor = true;
            // 
            // cbAnswer4
            // 
            this.cbAnswer4.AutoSize = true;
            this.cbAnswer4.Enabled = false;
            this.cbAnswer4.Location = new System.Drawing.Point(13, 130);
            this.cbAnswer4.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbAnswer4.Name = "cbAnswer4";
            this.cbAnswer4.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer4.TabIndex = 5;
            this.cbAnswer4.TabStop = false;
            this.cbAnswer4.Text = "Ответ 4";
            this.cbAnswer4.UseVisualStyleBackColor = true;
            // 
            // cbAnswer3
            // 
            this.cbAnswer3.AutoSize = true;
            this.cbAnswer3.Enabled = false;
            this.cbAnswer3.Location = new System.Drawing.Point(13, 103);
            this.cbAnswer3.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbAnswer3.Name = "cbAnswer3";
            this.cbAnswer3.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer3.TabIndex = 4;
            this.cbAnswer3.TabStop = false;
            this.cbAnswer3.Text = "Ответ 3";
            this.cbAnswer3.UseVisualStyleBackColor = true;
            // 
            // cbAnswer2
            // 
            this.cbAnswer2.AutoSize = true;
            this.cbAnswer2.Enabled = false;
            this.cbAnswer2.Location = new System.Drawing.Point(13, 76);
            this.cbAnswer2.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbAnswer2.Name = "cbAnswer2";
            this.cbAnswer2.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer2.TabIndex = 3;
            this.cbAnswer2.TabStop = false;
            this.cbAnswer2.Text = "Ответ 2";
            this.cbAnswer2.UseVisualStyleBackColor = true;
            // 
            // cbAnswer1
            // 
            this.cbAnswer1.AutoSize = true;
            this.cbAnswer1.Enabled = false;
            this.cbAnswer1.Location = new System.Drawing.Point(13, 49);
            this.cbAnswer1.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbAnswer1.Name = "cbAnswer1";
            this.cbAnswer1.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer1.TabIndex = 2;
            this.cbAnswer1.TabStop = false;
            this.cbAnswer1.Text = "Ответ 1";
            this.cbAnswer1.UseVisualStyleBackColor = true;
            // 
            // tbUserInputAnswer
            // 
            this.tbUserInputAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUserInputAnswer.Enabled = false;
            this.tbUserInputAnswer.Location = new System.Drawing.Point(118, 13);
            this.tbUserInputAnswer.Margin = new System.Windows.Forms.Padding(5, 10, 10, 10);
            this.tbUserInputAnswer.Name = "tbUserInputAnswer";
            this.tbUserInputAnswer.Size = new System.Drawing.Size(605, 26);
            this.tbUserInputAnswer.TabIndex = 1;
            this.tbUserInputAnswer.TabStop = false;
            // 
            // lUserInputAnswer
            // 
            this.lUserInputAnswer.AutoSize = true;
            this.lUserInputAnswer.Location = new System.Drawing.Point(10, 16);
            this.lUserInputAnswer.Margin = new System.Windows.Forms.Padding(0);
            this.lUserInputAnswer.Name = "lUserInputAnswer";
            this.lUserInputAnswer.Size = new System.Drawing.Size(103, 18);
            this.lUserInputAnswer.TabIndex = 0;
            this.lUserInputAnswer.Text = "Вы ответили:";
            // 
            // tpCorrectAnswers
            // 
            this.tpCorrectAnswers.Controls.Add(this.lInputAnswer);
            this.tpCorrectAnswers.Controls.Add(this.cbCorrectAnswer6);
            this.tpCorrectAnswers.Controls.Add(this.cbCorrectAnswer5);
            this.tpCorrectAnswers.Controls.Add(this.cbCorrectAnswer4);
            this.tpCorrectAnswers.Controls.Add(this.cbCorrectAnswer3);
            this.tpCorrectAnswers.Controls.Add(this.cbCorrectAnswer2);
            this.tpCorrectAnswers.Controls.Add(this.cbCorrectAnswer1);
            this.tpCorrectAnswers.Location = new System.Drawing.Point(4, 27);
            this.tpCorrectAnswers.Name = "tpCorrectAnswers";
            this.tpCorrectAnswers.Padding = new System.Windows.Forms.Padding(3);
            this.tpCorrectAnswers.Size = new System.Drawing.Size(736, 250);
            this.tpCorrectAnswers.TabIndex = 1;
            this.tpCorrectAnswers.Text = "Правильные ответы";
            this.tpCorrectAnswers.UseVisualStyleBackColor = true;
            // 
            // lInputAnswer
            // 
            this.lInputAnswer.AutoSize = true;
            this.lInputAnswer.Location = new System.Drawing.Point(13, 13);
            this.lInputAnswer.Margin = new System.Windows.Forms.Padding(10);
            this.lInputAnswer.Name = "lInputAnswer";
            this.lInputAnswer.Size = new System.Drawing.Size(638, 18);
            this.lInputAnswer.TabIndex = 19;
            this.lInputAnswer.Text = "В данном вопросе нет правильного ответа. Ваш ответ будет обработан проверяющим.";
            this.lInputAnswer.Visible = false;
            // 
            // cbCorrectAnswer6
            // 
            this.cbCorrectAnswer6.AutoSize = true;
            this.cbCorrectAnswer6.Enabled = false;
            this.cbCorrectAnswer6.Location = new System.Drawing.Point(13, 184);
            this.cbCorrectAnswer6.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbCorrectAnswer6.Name = "cbCorrectAnswer6";
            this.cbCorrectAnswer6.Size = new System.Drawing.Size(83, 22);
            this.cbCorrectAnswer6.TabIndex = 13;
            this.cbCorrectAnswer6.TabStop = false;
            this.cbCorrectAnswer6.Text = "Ответ 6";
            this.cbCorrectAnswer6.UseVisualStyleBackColor = true;
            // 
            // cbCorrectAnswer5
            // 
            this.cbCorrectAnswer5.AutoSize = true;
            this.cbCorrectAnswer5.Enabled = false;
            this.cbCorrectAnswer5.Location = new System.Drawing.Point(13, 157);
            this.cbCorrectAnswer5.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbCorrectAnswer5.Name = "cbCorrectAnswer5";
            this.cbCorrectAnswer5.Size = new System.Drawing.Size(83, 22);
            this.cbCorrectAnswer5.TabIndex = 12;
            this.cbCorrectAnswer5.TabStop = false;
            this.cbCorrectAnswer5.Text = "Ответ 5";
            this.cbCorrectAnswer5.UseVisualStyleBackColor = true;
            // 
            // cbCorrectAnswer4
            // 
            this.cbCorrectAnswer4.AutoSize = true;
            this.cbCorrectAnswer4.Enabled = false;
            this.cbCorrectAnswer4.Location = new System.Drawing.Point(13, 130);
            this.cbCorrectAnswer4.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbCorrectAnswer4.Name = "cbCorrectAnswer4";
            this.cbCorrectAnswer4.Size = new System.Drawing.Size(83, 22);
            this.cbCorrectAnswer4.TabIndex = 11;
            this.cbCorrectAnswer4.TabStop = false;
            this.cbCorrectAnswer4.Text = "Ответ 4";
            this.cbCorrectAnswer4.UseVisualStyleBackColor = true;
            // 
            // cbCorrectAnswer3
            // 
            this.cbCorrectAnswer3.AutoSize = true;
            this.cbCorrectAnswer3.Enabled = false;
            this.cbCorrectAnswer3.Location = new System.Drawing.Point(13, 103);
            this.cbCorrectAnswer3.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbCorrectAnswer3.Name = "cbCorrectAnswer3";
            this.cbCorrectAnswer3.Size = new System.Drawing.Size(83, 22);
            this.cbCorrectAnswer3.TabIndex = 10;
            this.cbCorrectAnswer3.TabStop = false;
            this.cbCorrectAnswer3.Text = "Ответ 3";
            this.cbCorrectAnswer3.UseVisualStyleBackColor = true;
            // 
            // cbCorrectAnswer2
            // 
            this.cbCorrectAnswer2.AutoSize = true;
            this.cbCorrectAnswer2.Enabled = false;
            this.cbCorrectAnswer2.Location = new System.Drawing.Point(13, 76);
            this.cbCorrectAnswer2.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbCorrectAnswer2.Name = "cbCorrectAnswer2";
            this.cbCorrectAnswer2.Size = new System.Drawing.Size(83, 22);
            this.cbCorrectAnswer2.TabIndex = 9;
            this.cbCorrectAnswer2.TabStop = false;
            this.cbCorrectAnswer2.Text = "Ответ 2";
            this.cbCorrectAnswer2.UseVisualStyleBackColor = true;
            // 
            // cbCorrectAnswer1
            // 
            this.cbCorrectAnswer1.AutoSize = true;
            this.cbCorrectAnswer1.Enabled = false;
            this.cbCorrectAnswer1.Location = new System.Drawing.Point(13, 49);
            this.cbCorrectAnswer1.Margin = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.cbCorrectAnswer1.Name = "cbCorrectAnswer1";
            this.cbCorrectAnswer1.Size = new System.Drawing.Size(83, 22);
            this.cbCorrectAnswer1.TabIndex = 8;
            this.cbCorrectAnswer1.TabStop = false;
            this.cbCorrectAnswer1.Text = "Ответ 1";
            this.cbCorrectAnswer1.UseVisualStyleBackColor = true;
            // 
            // tbQuestion
            // 
            this.tbQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbQuestion.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbQuestion.Location = new System.Drawing.Point(10, 70);
            this.tbQuestion.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.tbQuestion.Multiline = true;
            this.tbQuestion.Name = "tbQuestion";
            this.tbQuestion.ReadOnly = true;
            this.tbQuestion.Size = new System.Drawing.Size(744, 50);
            this.tbQuestion.TabIndex = 1;
            this.tbQuestion.TabStop = false;
            // 
            // tbTopic
            // 
            this.tbTopic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTopic.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbTopic.Location = new System.Drawing.Point(10, 10);
            this.tbTopic.Margin = new System.Windows.Forms.Padding(10);
            this.tbTopic.Multiline = true;
            this.tbTopic.Name = "tbTopic";
            this.tbTopic.ReadOnly = true;
            this.tbTopic.Size = new System.Drawing.Size(744, 50);
            this.tbTopic.TabIndex = 0;
            this.tbTopic.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel2.Controls.Add(this.lQuestionNumber, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lCorrectAnswer, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(9, 527);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(766, 25);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // lQuestionNumber
            // 
            this.lQuestionNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lQuestionNumber.AutoSize = true;
            this.lQuestionNumber.Location = new System.Drawing.Point(4, 3);
            this.lQuestionNumber.Name = "lQuestionNumber";
            this.lQuestionNumber.Size = new System.Drawing.Size(62, 18);
            this.lQuestionNumber.TabIndex = 0;
            this.lQuestionNumber.Text = "Вопрос";
            // 
            // lCorrectAnswer
            // 
            this.lCorrectAnswer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lCorrectAnswer.AutoSize = true;
            this.lCorrectAnswer.Location = new System.Drawing.Point(316, 3);
            this.lCorrectAnswer.Name = "lCorrectAnswer";
            this.lCorrectAnswer.Size = new System.Drawing.Size(286, 18);
            this.lCorrectAnswer.TabIndex = 1;
            this.lCorrectAnswer.Text = "Вы ответили правильно / неправильно";
            // 
            // TestResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.pTestInformation);
            this.Controls.Add(this.tlpStatistics);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TestResultForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TestOffice";
            this.tlpStatistics.ResumeLayout(false);
            this.tlpStatistics.PerformLayout();
            this.pTestInformation.ResumeLayout(false);
            this.pTestInformation.PerformLayout();
            this.tcAnswers.ResumeLayout(false);
            this.tpUserAnswers.ResumeLayout(false);
            this.tpUserAnswers.PerformLayout();
            this.tpCorrectAnswers.ResumeLayout(false);
            this.tpCorrectAnswers.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpStatistics;
        private System.Windows.Forms.Label lNumberOfCorrectAnswers;
        private System.Windows.Forms.Label lNumberOfIncorrectAnswers;
        private System.Windows.Forms.Label lUnansweredQuestions;
        private System.Windows.Forms.Panel pTestInformation;
        private System.Windows.Forms.Button btnNextQuestion;
        private System.Windows.Forms.Button btnPreviousQuestion;
        private System.Windows.Forms.TabControl tcAnswers;
        private System.Windows.Forms.TabPage tpUserAnswers;
        private System.Windows.Forms.TabPage tpCorrectAnswers;
        private System.Windows.Forms.TextBox tbQuestion;
        private System.Windows.Forms.TextBox tbTopic;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lQuestionNumber;
        private System.Windows.Forms.Label lCorrectAnswer;
        private System.Windows.Forms.TextBox tbUserInputAnswer;
        private System.Windows.Forms.Label lUserInputAnswer;
        private System.Windows.Forms.CheckBox cbAnswer6;
        private System.Windows.Forms.CheckBox cbAnswer5;
        private System.Windows.Forms.CheckBox cbAnswer4;
        private System.Windows.Forms.CheckBox cbAnswer3;
        private System.Windows.Forms.CheckBox cbAnswer2;
        private System.Windows.Forms.CheckBox cbAnswer1;
        private System.Windows.Forms.Label lInputAnswer;
        private System.Windows.Forms.CheckBox cbCorrectAnswer6;
        private System.Windows.Forms.CheckBox cbCorrectAnswer5;
        private System.Windows.Forms.CheckBox cbCorrectAnswer4;
        private System.Windows.Forms.CheckBox cbCorrectAnswer3;
        private System.Windows.Forms.CheckBox cbCorrectAnswer2;
        private System.Windows.Forms.CheckBox cbCorrectAnswer1;
    }
}