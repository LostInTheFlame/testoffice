﻿namespace TestOffice
{
    public class GeneralInformation
    {
        public string UserFullName { get; set; }
        public string Topic { get; set; }
        public int NumberOfCorrectAnswers { get; set; }
        public int NumberOfIncorrectAnswers { get; set; }
        public int NumberOfUnaccountedAnswers { get; set; }

        public GeneralInformation(string userFullName, string topic, int numberOfCorrectAnswers, int numberOfIncorrectAnswers, int numberOfUnaccountedAnswers)
        {
            UserFullName = userFullName;
            Topic = topic;
            NumberOfCorrectAnswers = numberOfCorrectAnswers;
            NumberOfIncorrectAnswers = numberOfIncorrectAnswers;
            NumberOfUnaccountedAnswers = numberOfUnaccountedAnswers;
        }
    }

    public class TestQuestionStructure
    {
        public string Question { get; set; }
        public bool CheckedAnswer1 { get; set; }
        public string TextAnswer1 { get; set; }
        public bool CheckedAnswer2 { get; set; }
        public string TextAnswer2 { get; set; }
        public bool CheckedAnswer3 { get; set; }
        public string TextAnswer3 { get; set; }
        public bool CheckedAnswer4 { get; set; }
        public string TextAnswer4 { get; set; }
        public bool CheckedAnswer5 { get; set; }
        public string TextAnswer5 { get; set; }
        public bool CheckedAnswer6 { get; set; }
        public string TextAnswer6 { get; set; }
        public bool CheckedUserInputAnswer { get; set; }

        public TestQuestionStructure(string question, bool checkedAnswer1, string textAnswer1, bool checkedAnswer2, string textAnswer2, bool checkedAnswer3, string textAnswer3, bool checkedAnswer4, string textAnswer4, bool checkedAnswer5, string textAnswer5, bool checkedAnswer6, string textAnswer6, bool checkedUserInputAnswer)
        {
            Question = question;
            CheckedAnswer1 = checkedAnswer1;
            TextAnswer1 = textAnswer1;
            CheckedAnswer2 = checkedAnswer2;
            TextAnswer2 = textAnswer2;
            CheckedAnswer3 = checkedAnswer3;
            TextAnswer3 = textAnswer3;
            CheckedAnswer4 = checkedAnswer4;
            TextAnswer4 = textAnswer4;
            CheckedAnswer5 = checkedAnswer5;
            TextAnswer5 = textAnswer5;
            CheckedAnswer6 = checkedAnswer6;
            TextAnswer6 = textAnswer6;
            CheckedUserInputAnswer = checkedUserInputAnswer;
        }

    }

    public class UserAnswerStructure
    {
        public bool CorrectAnswer { get; set; }
        public string UserInputAnswer { get; set; }
        public bool CheckedAnswer1 { get; set; }
        public bool CheckedAnswer2 { get; set; }
        public bool CheckedAnswer3 { get; set; }
        public bool CheckedAnswer4 { get; set; }
        public bool CheckedAnswer5 { get; set; }
        public bool CheckedAnswer6 { get; set; }

        public UserAnswerStructure(bool correctAnswer, string userInputAnswer, bool checkedAnswer1, bool checkedAnswer2, bool checkedAnswer3, bool checkedAnswer4, bool checkedAnswer5, bool checkedAnswer6)
        {
            CorrectAnswer = correctAnswer;
            UserInputAnswer = userInputAnswer;
            CheckedAnswer1 = checkedAnswer1;
            CheckedAnswer2 = checkedAnswer2;
            CheckedAnswer3 = checkedAnswer3;
            CheckedAnswer4 = checkedAnswer4;
            CheckedAnswer5 = checkedAnswer5;
            CheckedAnswer6 = checkedAnswer6;
        }
    }
}
