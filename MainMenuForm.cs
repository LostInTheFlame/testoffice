﻿using System;
using System.IO;
using System.Windows.Forms;

namespace TestOffice
{
    public partial class MainMenuForm : Form
    {
        public MainMenuForm()
        {
            InitializeComponent();
        }

        private void MainMenuForm_Load(object sender, EventArgs e)
        {
            if (!Properties.Settings.Default.DontShowAboutProgramForm)
            {
                new AboutProgramForm().ShowDialog();
            }

            LoadUserTestResultFileNames();
            EnablingAndDisablingElements();
        }

        private void tsmiAboutProgram_Click(object sender, EventArgs e)
        {
            new AboutProgramForm().ShowDialog();
        }

        private void tsmiCreateNewTest_Click(object sender, EventArgs e)
        {
            CreateNewTest();
        }

        private void tsmiEditTest_Click(object sender, EventArgs e)
        {
            EditTest();
        }

        private void tsmiTakeTest_Click(object sender, EventArgs e)
        {
            TakeTest();
        }

        private void LoadUserTestResultFileNames()
        {
            lbListUserTestResults.Items.Clear();
            Directory.CreateDirectory(@".\Test_results");

            foreach (FileInfo file in new DirectoryInfo(@".\Test_results\").GetFiles(@"*.json"))
            {
                lbListUserTestResults.Items.Add(Path.GetFileNameWithoutExtension(file.FullName));
            }
        }

        private void btnViewTestResults_Click(object sender, EventArgs e)
        {
            TestResultForm testResultForm = new TestResultForm();
            testResultForm.OpenTestResults($"{lbListUserTestResults.SelectedItem.ToString()}.json");
            testResultForm.ShowDialog();
        }

        private void btnDeleteTestResults_Click(object sender, EventArgs e)
        {
            File.Delete($@".\Test_results\{lbListUserTestResults.SelectedItem.ToString()}.json");
            LoadUserTestResultFileNames();
            EnablingAndDisablingElements();
        }

        private void btnCreateNewTest_Click(object sender, EventArgs e)
        {
            CreateNewTest();
        }

        private void btnEditTest_Click(object sender, EventArgs e)
        {
            EditTest();
        }

        private void btnTakeTest_Click(object sender, EventArgs e)
        {
            TakeTest();
        }

        private void CreateNewTest()
        {
            new TestCreationAndEditingForm().ShowDialog();
        }

        private void EditTest()
        {
            TestCreationAndEditingForm testCreationAndEditingForm = new TestCreationAndEditingForm();
            testCreationAndEditingForm.OpenTest();

            if (!testCreationAndEditingForm.IsDisposed)
            {
                testCreationAndEditingForm.ShowDialog();
            }
        }

        private void TakeTest()
        {
            TestPassingForm testPassingForm = new TestPassingForm();
            testPassingForm.OpenTest();

            if (!testPassingForm.IsDisposed)
            {
                testPassingForm.ShowDialog();
            }
        }

        private void lbListUserTestResults_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnablingAndDisablingElements();
        }

        private void EnablingAndDisablingElements()
        {
            btnViewTestResults.Enabled = btnDeleteTestResults.Enabled = lbListUserTestResults.SelectedIndex >= 0;
        }

        private void MainMenuForm_Activated(object sender, EventArgs e)
        {
            LoadUserTestResultFileNames();
            EnablingAndDisablingElements();
        }
    }
}
