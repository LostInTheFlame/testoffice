﻿using Newtonsoft.Json;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace TestOffice
{
    public partial class TestResultForm : Form
    {
        TestPassingStructure testPassingStructure = new TestPassingStructure();
        int questionNumber = 0;

        public TestResultForm()
        {
            InitializeComponent();
        }

        public void OpenTestResults(string userTestResultsFileName)
        {
            testPassingStructure = JsonConvert.DeserializeObject<TestPassingStructure>(File.ReadAllText($@".\Test_results\{userTestResultsFileName}"));
            EnablingAndDisablingElements();
            FillOutFields();
        }

        private void FillOutFields()
        {
            foreach (GeneralInformation item in testPassingStructure.GeneralInformation)
            {
                lNumberOfCorrectAnswers.Text = "Правильных ответов: " + item.NumberOfCorrectAnswers;
                lNumberOfIncorrectAnswers.Text = "Неправильных ответов: " + item.NumberOfIncorrectAnswers;
                lUnansweredQuestions.Text = "Неотвеченных вопросов: " + (testPassingStructure.TestQuestions.Count - (item.NumberOfCorrectAnswers + item.NumberOfIncorrectAnswers + item.NumberOfUnaccountedAnswers));
                tbTopic.Text = item.Topic;
            }

            tbQuestion.Text = testPassingStructure.TestQuestions[questionNumber].Question;
            tbUserInputAnswer.Text = testPassingStructure.UserAnswers[questionNumber].UserInputAnswer;
            cbAnswer1.Text = cbCorrectAnswer1.Text = testPassingStructure.TestQuestions[questionNumber].TextAnswer1;
            cbAnswer2.Text = cbCorrectAnswer2.Text = testPassingStructure.TestQuestions[questionNumber].TextAnswer2;
            cbAnswer3.Text = cbCorrectAnswer3.Text = testPassingStructure.TestQuestions[questionNumber].TextAnswer3;
            cbAnswer4.Text = cbCorrectAnswer4.Text = testPassingStructure.TestQuestions[questionNumber].TextAnswer4;
            cbAnswer5.Text = cbCorrectAnswer5.Text = testPassingStructure.TestQuestions[questionNumber].TextAnswer5;
            cbAnswer6.Text = cbCorrectAnswer6.Text = testPassingStructure.TestQuestions[questionNumber].TextAnswer6;
            cbAnswer1.Checked = testPassingStructure.UserAnswers[questionNumber].CheckedAnswer1;
            cbAnswer2.Checked = testPassingStructure.UserAnswers[questionNumber].CheckedAnswer2;
            cbAnswer3.Checked = testPassingStructure.UserAnswers[questionNumber].CheckedAnswer3;
            cbAnswer4.Checked = testPassingStructure.UserAnswers[questionNumber].CheckedAnswer4;
            cbAnswer5.Checked = testPassingStructure.UserAnswers[questionNumber].CheckedAnswer5;
            cbAnswer6.Checked = testPassingStructure.UserAnswers[questionNumber].CheckedAnswer6;
            cbCorrectAnswer1.Checked = testPassingStructure.TestQuestions[questionNumber].CheckedAnswer1;
            cbCorrectAnswer2.Checked = testPassingStructure.TestQuestions[questionNumber].CheckedAnswer2;
            cbCorrectAnswer3.Checked = testPassingStructure.TestQuestions[questionNumber].CheckedAnswer3;
            cbCorrectAnswer4.Checked = testPassingStructure.TestQuestions[questionNumber].CheckedAnswer4;
            cbCorrectAnswer5.Checked = testPassingStructure.TestQuestions[questionNumber].CheckedAnswer5;
            cbCorrectAnswer6.Checked = testPassingStructure.TestQuestions[questionNumber].CheckedAnswer6;
            lQuestionNumber.Text = $"Вопрос {questionNumber + 1} из {testPassingStructure.UserAnswers.Count}";

            if (testPassingStructure.UserAnswers[questionNumber].CorrectAnswer)
            {
                lCorrectAnswer.ForeColor = Color.Green;
                lCorrectAnswer.Text = "Вы ответили правильно";
            }

            else if (testPassingStructure.TestQuestions[questionNumber].CheckedUserInputAnswer && testPassingStructure.UserAnswers[questionNumber].UserInputAnswer != string.Empty)
            {
                lCorrectAnswer.ForeColor = Color.Blue;
                lCorrectAnswer.Text = "Данный ответ не идет в статистку правильных или неправильных ответов";
            }

            else if (!cbAnswer1.Checked && !cbAnswer2.Checked && !cbAnswer3.Checked && !cbAnswer4.Checked && !cbAnswer5.Checked && !cbAnswer6.Checked && tbUserInputAnswer.Text == string.Empty)
            {
                lCorrectAnswer.ForeColor = Color.Blue;
                lCorrectAnswer.Text = "Вы не ответили на данный вопрос";
            }

            else
            {
                lCorrectAnswer.ForeColor = Color.Red;
                lCorrectAnswer.Text = "Вы ответили неправильно";
            }
        }

        private void EnablingAndDisablingElements()
        {
            lUserInputAnswer.Visible = tbUserInputAnswer.Visible = lInputAnswer.Visible = testPassingStructure.TestQuestions[questionNumber].CheckedUserInputAnswer;
            cbAnswer1.Visible = cbCorrectAnswer1.Visible = testPassingStructure.TestQuestions[questionNumber].TextAnswer1 != string.Empty;
            cbAnswer2.Visible = cbCorrectAnswer2.Visible = testPassingStructure.TestQuestions[questionNumber].TextAnswer2 != string.Empty;
            cbAnswer3.Visible = cbCorrectAnswer3.Visible = testPassingStructure.TestQuestions[questionNumber].TextAnswer3 != string.Empty;
            cbAnswer4.Visible = cbCorrectAnswer4.Visible = testPassingStructure.TestQuestions[questionNumber].TextAnswer4 != string.Empty;
            cbAnswer5.Visible = cbCorrectAnswer5.Visible = testPassingStructure.TestQuestions[questionNumber].TextAnswer5 != string.Empty;
            cbAnswer6.Visible = cbCorrectAnswer6.Visible = testPassingStructure.TestQuestions[questionNumber].TextAnswer6 != string.Empty;
        }

        private void btnNextQuestion_Click(object sender, EventArgs e)
        {
            if (questionNumber < testPassingStructure.UserAnswers.Count - 1)
            {
                questionNumber++;
                EnablingAndDisablingElements();
                FillOutFields();
            }
        }

        private void btnPreviousQuestion_Click(object sender, EventArgs e)
        {
            if (questionNumber > 0)
            {
                questionNumber--;
                EnablingAndDisablingElements();
                FillOutFields();
            }
        }
    }
}
