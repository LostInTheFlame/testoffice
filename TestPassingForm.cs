﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace TestOffice
{
    public partial class TestPassingForm : Form
    {
        TestStructure testStructure = new TestStructure();
        TestPassingStructure testPassingStructure = new TestPassingStructure();
        int questionNumber = 0;
        bool correctAnswer = false;
        int numberOfCorrectAnswers = 0;
        int numberOfIncorrectAnswers = 0;
        int numberOfUnaccountedAnswers = 0;
        int minutes = 0;
        int seconds = 0;

        public TestPassingForm()
        {
            InitializeComponent();
        }

        public void OpenTest()
        {
            ofdOpenTest.Filter = "JSON файлы (*.json)|*.json";
            ofdOpenTest.RestoreDirectory = true;

            if (ofdOpenTest.ShowDialog() == DialogResult.Cancel)
            {
                this.Close();
                return;
            }

            testStructure = JsonConvert.DeserializeObject<TestStructure>(File.ReadAllText(ofdOpenTest.FileName));
        }

        private void btnStartTest_Click(object sender, EventArgs e)
        {
            if (tbUserFullName.Text == string.Empty)
            {
                MessageBox.Show("Введите ФИО.", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            EnablingAndDisablingElements();
            FillOutFields();
            InitializingTimers();
        }

        private void btnNextQuestion_Click(object sender, EventArgs e)
        {
            if ((!cbAnswer1.Checked && !cbAnswer2.Checked && !cbAnswer3.Checked && !cbAnswer4.Checked && !cbAnswer5.Checked && !cbAnswer6.Checked && !testStructure.Questions[questionNumber].CheckedUserInputAnswer) ^ (testStructure.Questions[questionNumber].CheckedUserInputAnswer && tbUserInputAnswer.Text == string.Empty))
            {
                MessageBox.Show("Вы не ответили на вопрос", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            CheckingAnswer();
            AddToList();
            ShowCorrectOrInccorectAnswer();

            if (questionNumber < testStructure.Questions.Count - 1)
            {
                questionNumber++;
                FillOutFields();
                ClearFields();
                EnablingAndDisablingElements();
            }

            else
            {
                SaveResult();
            }
        }

        private void FillOutFields()
        {
            foreach (GeneralSettings item in testStructure.GeneralSettings)
            {
                tbTopic.Text = item.Topic;
            }

            tbQuestion.Text = testStructure.Questions[questionNumber].Question;
            cbAnswer1.Text = testStructure.Questions[questionNumber].TextAnswer1;
            cbAnswer2.Text = testStructure.Questions[questionNumber].TextAnswer2;
            cbAnswer3.Text = testStructure.Questions[questionNumber].TextAnswer3;
            cbAnswer4.Text = testStructure.Questions[questionNumber].TextAnswer4;
            cbAnswer5.Text = testStructure.Questions[questionNumber].TextAnswer5;
            cbAnswer6.Text = testStructure.Questions[questionNumber].TextAnswer6;
            lQuestionNumber.Text = $"Вопрос {questionNumber + 1} из {testStructure.Questions.Count}";
        }

        private void ClearFields()
        {
            pTest.Controls
                .OfType<CheckBox>()
                .Where(c => c.Tag?.ToString() == "CheckedAnswer")
                .ToList()
                .ForEach(checkBox => checkBox.Checked = false);
            tbUserInputAnswer.Text = string.Empty;
        }

        private void EnablingAndDisablingElements()
        {
            pStartTest.Enabled = false;
            pTest.Enabled = true;

            lUserInputAnswer.Enabled = lUserInputAnswer.Visible = tbUserInputAnswer.Enabled = tbUserInputAnswer.Visible = testStructure.Questions[questionNumber].CheckedUserInputAnswer;
            cbAnswer1.Enabled = cbAnswer1.Visible = testStructure.Questions[questionNumber].TextAnswer1 != string.Empty;
            cbAnswer2.Enabled = cbAnswer2.Visible = testStructure.Questions[questionNumber].TextAnswer2 != string.Empty;
            cbAnswer3.Enabled = cbAnswer3.Visible = testStructure.Questions[questionNumber].TextAnswer3 != string.Empty;
            cbAnswer4.Enabled = cbAnswer4.Visible = testStructure.Questions[questionNumber].TextAnswer4 != string.Empty;
            cbAnswer5.Enabled = cbAnswer5.Visible = testStructure.Questions[questionNumber].TextAnswer5 != string.Empty;
            cbAnswer6.Enabled = cbAnswer6.Visible = testStructure.Questions[questionNumber].TextAnswer6 != string.Empty;

            foreach (GeneralSettings item in testStructure.GeneralSettings)
            {
                lTimeLeft.Enabled = lTimeLeft.Visible = pbTimeLeft.Enabled = pbTimeLeft.Visible = item.ActivateTimer;
            }
        }

        private void CheckingAnswer()
        {
            if (
                (
                cbAnswer1.Checked == testStructure.Questions[questionNumber].CheckedAnswer1 &&
                cbAnswer2.Checked == testStructure.Questions[questionNumber].CheckedAnswer2 &&
                cbAnswer3.Checked == testStructure.Questions[questionNumber].CheckedAnswer3 &&
                cbAnswer4.Checked == testStructure.Questions[questionNumber].CheckedAnswer4 &&
                cbAnswer5.Checked == testStructure.Questions[questionNumber].CheckedAnswer5 &&
                cbAnswer6.Checked == testStructure.Questions[questionNumber].CheckedAnswer6
                )
                && !testStructure.Questions[questionNumber].CheckedUserInputAnswer
               )
            {
                correctAnswer = true;
                numberOfCorrectAnswers++;
            }

            else if (testStructure.Questions[questionNumber].CheckedUserInputAnswer && tbUserInputAnswer.Text != string.Empty)
            {
                correctAnswer = false;
                numberOfUnaccountedAnswers++;
            }

            else
            {
                correctAnswer = false;
                numberOfIncorrectAnswers++;
            }
        }

        private void ShowCorrectOrInccorectAnswer()
        {
            foreach (GeneralSettings item in testStructure.GeneralSettings)
            {
                if (!item.ShowCorrectOrIncorrectAnswer || testStructure.Questions[questionNumber].CheckedUserInputAnswer)
                {
                    return;
                }

                if (correctAnswer)
                {
                    MessageBox.Show("Вы ответили правильно.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                else
                {
                    MessageBox.Show("Вы ответили неправильно.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void InitializingTimers()
        {
            foreach (GeneralSettings item in testStructure.GeneralSettings)
            {
                if (!item.ActivateTimer)
                {
                    return;
                }

                if (Convert.ToInt32(item.SetTime) > 0)
                {
                    minutes = Convert.ToInt32(item.SetTime) - 1;
                    seconds = 60;
                }

                else
                {
                    minutes = Convert.ToInt32(item.SetTime);
                    seconds = minutes * 60;
                }

                tMainTimer.Interval = Convert.ToInt32(item.SetTime) * 60000;
                tMainTimer.Enabled = true;
                tUpdateTimeLeft.Interval = 1000;
                tUpdateTimeLeft.Enabled = true;
            }
        }

        private void tMainTimer_Tick(object sender, EventArgs e)
        {
            tMainTimer.Stop();

            while (questionNumber < testStructure.Questions.Count)
            {
                correctAnswer = false;
                AddToList();
                questionNumber++;
            }

            SaveResult();
        }

        private void tUpdateTimeLeft_Tick(object sender, EventArgs e)
        {
            seconds -= 1;
            ProgressTimeLeft();

            if (seconds == 0 && minutes > 0)
            {
                minutes -= 1;
                seconds = 60;
            }

            else if (minutes == 0 && seconds == 0)
            {
                tUpdateTimeLeft.Stop();
            }

            lTimeLeft.Text = $"Времени осталось: {minutes}:{seconds}";
        }

        private void ProgressTimeLeft()
        {
            pbTimeLeft.Minimum = 0;

            foreach (GeneralSettings item in testStructure.GeneralSettings)
            {
                pbTimeLeft.Maximum = Convert.ToInt32(item.SetTime) * 60;
            }

            pbTimeLeft.Step = 1;
            pbTimeLeft.PerformStep();
        }

        private void AddToList()
        {
            TestQuestionStructure testQuestionStructure = new TestQuestionStructure(testStructure.Questions[questionNumber].Question, testStructure.Questions[questionNumber].CheckedAnswer1, testStructure.Questions[questionNumber].TextAnswer1, testStructure.Questions[questionNumber].CheckedAnswer2, testStructure.Questions[questionNumber].TextAnswer2, testStructure.Questions[questionNumber].CheckedAnswer3, testStructure.Questions[questionNumber].TextAnswer3, testStructure.Questions[questionNumber].CheckedAnswer4, testStructure.Questions[questionNumber].TextAnswer4, testStructure.Questions[questionNumber].CheckedAnswer5, testStructure.Questions[questionNumber].TextAnswer5, testStructure.Questions[questionNumber].CheckedAnswer6, testStructure.Questions[questionNumber].TextAnswer6, testStructure.Questions[questionNumber].CheckedUserInputAnswer);
            testPassingStructure.TestQuestions.Add(testQuestionStructure);
            UserAnswerStructure userAnswerStructure = new UserAnswerStructure(correctAnswer, tbUserInputAnswer.Text, cbAnswer1.Checked, cbAnswer2.Checked, cbAnswer3.Checked, cbAnswer4.Checked, cbAnswer5.Checked, cbAnswer6.Checked);
            testPassingStructure.UserAnswers.Add(userAnswerStructure);
        }

        private void SaveResult()
        {
            tMainTimer.Stop();
            tUpdateTimeLeft.Stop();
            testPassingStructure.GeneralInformation.Clear();
            GeneralInformation generalInformation = new GeneralInformation(tbUserFullName.Text, tbTopic.Text, numberOfCorrectAnswers, numberOfIncorrectAnswers, numberOfUnaccountedAnswers);
            testPassingStructure.GeneralInformation.Add(generalInformation);
            Directory.CreateDirectory(@".\Test_results");
            string userTestResultsFileName = $"{tbUserFullName.Text} - {Path.GetFileName(ofdOpenTest.FileName)}";
            File.WriteAllText($@".\Test_results\{userTestResultsFileName}", JsonConvert.SerializeObject(testPassingStructure, Formatting.Indented));
            this.Hide();
            TestResultForm testResultForm = new TestResultForm();
            testResultForm.OpenTestResults(userTestResultsFileName);
            testResultForm.ShowDialog();
            this.Close();
        }

        private void TestPassingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            tMainTimer.Stop();
            tUpdateTimeLeft.Stop();
        }
    }
}
