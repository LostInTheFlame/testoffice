﻿using System.Collections.Generic;

namespace TestOffice
{
    public class TestStructure
    {
        public List<GeneralSettings> GeneralSettings { get; set; } = new List<GeneralSettings>();
        public List<QuestionStructure> Questions { get; set; } = new List<QuestionStructure>();
    }

    public class TestPassingStructure
    {
        public List<GeneralInformation> GeneralInformation { get; set; } = new List<GeneralInformation>();
        public List<TestQuestionStructure> TestQuestions { get; set; } = new List<TestQuestionStructure>();
        public List<UserAnswerStructure> UserAnswers { get; set; } = new List<UserAnswerStructure>();
    }
}
