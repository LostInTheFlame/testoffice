﻿namespace TestOffice
{
    partial class TestPassingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pStartTest = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnStartTest = new System.Windows.Forms.Button();
            this.tbUserFullName = new System.Windows.Forms.TextBox();
            this.lFullName = new System.Windows.Forms.Label();
            this.pTest = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnNextQuestion = new System.Windows.Forms.Button();
            this.cbAnswer6 = new System.Windows.Forms.CheckBox();
            this.cbAnswer5 = new System.Windows.Forms.CheckBox();
            this.cbAnswer4 = new System.Windows.Forms.CheckBox();
            this.cbAnswer3 = new System.Windows.Forms.CheckBox();
            this.cbAnswer2 = new System.Windows.Forms.CheckBox();
            this.cbAnswer1 = new System.Windows.Forms.CheckBox();
            this.tbUserInputAnswer = new System.Windows.Forms.TextBox();
            this.lUserInputAnswer = new System.Windows.Forms.Label();
            this.tbQuestion = new System.Windows.Forms.TextBox();
            this.tbTopic = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lTimeLeft = new System.Windows.Forms.Label();
            this.lQuestionNumber = new System.Windows.Forms.Label();
            this.pbTimeLeft = new System.Windows.Forms.ProgressBar();
            this.tMainTimer = new System.Windows.Forms.Timer(this.components);
            this.tUpdateTimeLeft = new System.Windows.Forms.Timer(this.components);
            this.ofdOpenTest = new System.Windows.Forms.OpenFileDialog();
            this.pStartTest.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.pTest.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pStartTest
            // 
            this.pStartTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pStartTest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pStartTest.Controls.Add(this.tableLayoutPanel1);
            this.pStartTest.Controls.Add(this.tbUserFullName);
            this.pStartTest.Controls.Add(this.lFullName);
            this.pStartTest.Location = new System.Drawing.Point(9, 9);
            this.pStartTest.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.pStartTest.Name = "pStartTest";
            this.pStartTest.Size = new System.Drawing.Size(766, 96);
            this.pStartTest.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnStartTest, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 46);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(744, 40);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // btnStartTest
            // 
            this.btnStartTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnStartTest.Location = new System.Drawing.Point(282, 0);
            this.btnStartTest.Margin = new System.Windows.Forms.Padding(0);
            this.btnStartTest.Name = "btnStartTest";
            this.btnStartTest.Size = new System.Drawing.Size(180, 40);
            this.btnStartTest.TabIndex = 2;
            this.btnStartTest.TabStop = false;
            this.btnStartTest.Text = "Начать тестирование";
            this.btnStartTest.UseVisualStyleBackColor = true;
            this.btnStartTest.Click += new System.EventHandler(this.btnStartTest_Click);
            // 
            // tbUserFullName
            // 
            this.tbUserFullName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUserFullName.Location = new System.Drawing.Point(125, 10);
            this.tbUserFullName.Margin = new System.Windows.Forms.Padding(5, 10, 10, 10);
            this.tbUserFullName.Name = "tbUserFullName";
            this.tbUserFullName.Size = new System.Drawing.Size(629, 26);
            this.tbUserFullName.TabIndex = 1;
            this.tbUserFullName.TabStop = false;
            // 
            // lFullName
            // 
            this.lFullName.AutoSize = true;
            this.lFullName.Location = new System.Drawing.Point(7, 13);
            this.lFullName.Margin = new System.Windows.Forms.Padding(0);
            this.lFullName.Name = "lFullName";
            this.lFullName.Size = new System.Drawing.Size(113, 18);
            this.lFullName.TabIndex = 0;
            this.lFullName.Text = "Введите ФИО:";
            // 
            // pTest
            // 
            this.pTest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pTest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTest.Controls.Add(this.tableLayoutPanel3);
            this.pTest.Controls.Add(this.cbAnswer6);
            this.pTest.Controls.Add(this.cbAnswer5);
            this.pTest.Controls.Add(this.cbAnswer4);
            this.pTest.Controls.Add(this.cbAnswer3);
            this.pTest.Controls.Add(this.cbAnswer2);
            this.pTest.Controls.Add(this.cbAnswer1);
            this.pTest.Controls.Add(this.tbUserInputAnswer);
            this.pTest.Controls.Add(this.lUserInputAnswer);
            this.pTest.Controls.Add(this.tbQuestion);
            this.pTest.Controls.Add(this.tbTopic);
            this.pTest.Enabled = false;
            this.pTest.Location = new System.Drawing.Point(9, 115);
            this.pTest.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.pTest.Name = "pTest";
            this.pTest.Size = new System.Drawing.Size(766, 397);
            this.pTest.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.btnNextQuestion, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(10, 345);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(744, 40);
            this.tableLayoutPanel3.TabIndex = 11;
            // 
            // btnNextQuestion
            // 
            this.btnNextQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextQuestion.Location = new System.Drawing.Point(584, 0);
            this.btnNextQuestion.Margin = new System.Windows.Forms.Padding(0);
            this.btnNextQuestion.Name = "btnNextQuestion";
            this.btnNextQuestion.Size = new System.Drawing.Size(160, 40);
            this.btnNextQuestion.TabIndex = 0;
            this.btnNextQuestion.TabStop = false;
            this.btnNextQuestion.Text = "Следующий вопрос";
            this.btnNextQuestion.UseVisualStyleBackColor = true;
            this.btnNextQuestion.Click += new System.EventHandler(this.btnNextQuestion_Click);
            // 
            // cbAnswer6
            // 
            this.cbAnswer6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnswer6.AutoSize = true;
            this.cbAnswer6.Location = new System.Drawing.Point(10, 301);
            this.cbAnswer6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.cbAnswer6.Name = "cbAnswer6";
            this.cbAnswer6.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer6.TabIndex = 9;
            this.cbAnswer6.TabStop = false;
            this.cbAnswer6.Tag = "CheckedAnswer";
            this.cbAnswer6.Text = "Ответ 6";
            this.cbAnswer6.UseVisualStyleBackColor = true;
            // 
            // cbAnswer5
            // 
            this.cbAnswer5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnswer5.AutoSize = true;
            this.cbAnswer5.Location = new System.Drawing.Point(10, 274);
            this.cbAnswer5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.cbAnswer5.Name = "cbAnswer5";
            this.cbAnswer5.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer5.TabIndex = 8;
            this.cbAnswer5.TabStop = false;
            this.cbAnswer5.Tag = "CheckedAnswer";
            this.cbAnswer5.Text = "Ответ 5";
            this.cbAnswer5.UseVisualStyleBackColor = true;
            // 
            // cbAnswer4
            // 
            this.cbAnswer4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnswer4.AutoSize = true;
            this.cbAnswer4.Location = new System.Drawing.Point(10, 247);
            this.cbAnswer4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.cbAnswer4.Name = "cbAnswer4";
            this.cbAnswer4.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer4.TabIndex = 7;
            this.cbAnswer4.TabStop = false;
            this.cbAnswer4.Tag = "CheckedAnswer";
            this.cbAnswer4.Text = "Ответ 4";
            this.cbAnswer4.UseVisualStyleBackColor = true;
            // 
            // cbAnswer3
            // 
            this.cbAnswer3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnswer3.AutoSize = true;
            this.cbAnswer3.Location = new System.Drawing.Point(10, 220);
            this.cbAnswer3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.cbAnswer3.Name = "cbAnswer3";
            this.cbAnswer3.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer3.TabIndex = 6;
            this.cbAnswer3.TabStop = false;
            this.cbAnswer3.Tag = "CheckedAnswer";
            this.cbAnswer3.Text = "Ответ 3";
            this.cbAnswer3.UseVisualStyleBackColor = true;
            // 
            // cbAnswer2
            // 
            this.cbAnswer2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnswer2.AutoSize = true;
            this.cbAnswer2.Location = new System.Drawing.Point(10, 193);
            this.cbAnswer2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.cbAnswer2.Name = "cbAnswer2";
            this.cbAnswer2.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer2.TabIndex = 5;
            this.cbAnswer2.TabStop = false;
            this.cbAnswer2.Tag = "CheckedAnswer";
            this.cbAnswer2.Text = "Ответ 2";
            this.cbAnswer2.UseVisualStyleBackColor = true;
            // 
            // cbAnswer1
            // 
            this.cbAnswer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnswer1.AutoSize = true;
            this.cbAnswer1.Location = new System.Drawing.Point(10, 166);
            this.cbAnswer1.Margin = new System.Windows.Forms.Padding(10, 0, 5, 5);
            this.cbAnswer1.Name = "cbAnswer1";
            this.cbAnswer1.Size = new System.Drawing.Size(83, 22);
            this.cbAnswer1.TabIndex = 4;
            this.cbAnswer1.TabStop = false;
            this.cbAnswer1.Tag = "CheckedAnswer";
            this.cbAnswer1.Text = "Ответ 1";
            this.cbAnswer1.UseVisualStyleBackColor = true;
            // 
            // tbUserInputAnswer
            // 
            this.tbUserInputAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUserInputAnswer.Location = new System.Drawing.Point(130, 130);
            this.tbUserInputAnswer.Margin = new System.Windows.Forms.Padding(5, 0, 10, 10);
            this.tbUserInputAnswer.Name = "tbUserInputAnswer";
            this.tbUserInputAnswer.Size = new System.Drawing.Size(624, 26);
            this.tbUserInputAnswer.TabIndex = 3;
            this.tbUserInputAnswer.TabStop = false;
            // 
            // lUserInputAnswer
            // 
            this.lUserInputAnswer.AutoSize = true;
            this.lUserInputAnswer.Location = new System.Drawing.Point(7, 133);
            this.lUserInputAnswer.Margin = new System.Windows.Forms.Padding(0);
            this.lUserInputAnswer.Name = "lUserInputAnswer";
            this.lUserInputAnswer.Size = new System.Drawing.Size(118, 18);
            this.lUserInputAnswer.TabIndex = 2;
            this.lUserInputAnswer.Text = "Введите ответ:";
            // 
            // tbQuestion
            // 
            this.tbQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbQuestion.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbQuestion.Location = new System.Drawing.Point(10, 70);
            this.tbQuestion.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.tbQuestion.Multiline = true;
            this.tbQuestion.Name = "tbQuestion";
            this.tbQuestion.ReadOnly = true;
            this.tbQuestion.Size = new System.Drawing.Size(744, 50);
            this.tbQuestion.TabIndex = 1;
            this.tbQuestion.TabStop = false;
            // 
            // tbTopic
            // 
            this.tbTopic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTopic.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbTopic.Location = new System.Drawing.Point(10, 10);
            this.tbTopic.Margin = new System.Windows.Forms.Padding(10);
            this.tbTopic.Multiline = true;
            this.tbTopic.Name = "tbTopic";
            this.tbTopic.ReadOnly = true;
            this.tbTopic.Size = new System.Drawing.Size(744, 50);
            this.tbTopic.TabIndex = 0;
            this.tbTopic.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.Controls.Add(this.lTimeLeft, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lQuestionNumber, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.pbTimeLeft, 2, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(9, 522);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(766, 30);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // lTimeLeft
            // 
            this.lTimeLeft.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lTimeLeft.AutoSize = true;
            this.lTimeLeft.Enabled = false;
            this.lTimeLeft.Location = new System.Drawing.Point(302, 6);
            this.lTimeLeft.Name = "lTimeLeft";
            this.lTimeLeft.Size = new System.Drawing.Size(153, 18);
            this.lTimeLeft.TabIndex = 0;
            this.lTimeLeft.Text = "Осталось времени: ";
            this.lTimeLeft.Visible = false;
            // 
            // lQuestionNumber
            // 
            this.lQuestionNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lQuestionNumber.AutoSize = true;
            this.lQuestionNumber.Location = new System.Drawing.Point(4, 6);
            this.lQuestionNumber.Name = "lQuestionNumber";
            this.lQuestionNumber.Size = new System.Drawing.Size(70, 18);
            this.lQuestionNumber.TabIndex = 1;
            this.lQuestionNumber.Text = "Вопрос: ";
            // 
            // pbTimeLeft
            // 
            this.pbTimeLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pbTimeLeft.Enabled = false;
            this.pbTimeLeft.Location = new System.Drawing.Point(462, 4);
            this.pbTimeLeft.Name = "pbTimeLeft";
            this.pbTimeLeft.Size = new System.Drawing.Size(300, 22);
            this.pbTimeLeft.TabIndex = 2;
            this.pbTimeLeft.Visible = false;
            // 
            // tMainTimer
            // 
            this.tMainTimer.Tick += new System.EventHandler(this.tMainTimer_Tick);
            // 
            // tUpdateTimeLeft
            // 
            this.tUpdateTimeLeft.Tick += new System.EventHandler(this.tUpdateTimeLeft_Tick);
            // 
            // ofdOpenTest
            // 
            this.ofdOpenTest.FileName = "openFileDialog1";
            // 
            // TestPassingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pTest);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.pStartTest);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TestPassingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TestOffice";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TestPassingForm_FormClosing);
            this.pStartTest.ResumeLayout(false);
            this.pStartTest.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.pTest.ResumeLayout(false);
            this.pTest.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pStartTest;
        private System.Windows.Forms.Button btnStartTest;
        private System.Windows.Forms.TextBox tbUserFullName;
        private System.Windows.Forms.Label lFullName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel pTest;
        private System.Windows.Forms.TextBox tbUserInputAnswer;
        private System.Windows.Forms.Label lUserInputAnswer;
        private System.Windows.Forms.TextBox tbQuestion;
        private System.Windows.Forms.TextBox tbTopic;
        private System.Windows.Forms.CheckBox cbAnswer1;
        private System.Windows.Forms.CheckBox cbAnswer6;
        private System.Windows.Forms.CheckBox cbAnswer5;
        private System.Windows.Forms.CheckBox cbAnswer4;
        private System.Windows.Forms.CheckBox cbAnswer3;
        private System.Windows.Forms.CheckBox cbAnswer2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnNextQuestion;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lTimeLeft;
        private System.Windows.Forms.Label lQuestionNumber;
        private System.Windows.Forms.ProgressBar pbTimeLeft;
        private System.Windows.Forms.Timer tMainTimer;
        private System.Windows.Forms.Timer tUpdateTimeLeft;
        private System.Windows.Forms.OpenFileDialog ofdOpenTest;
    }
}