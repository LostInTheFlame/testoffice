﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace TestOffice
{
    public partial class TestCreationAndEditingForm : Form
    {
        TestStructure testStructure = new TestStructure();

        public TestCreationAndEditingForm()
        {
            InitializeComponent();
        }

        private void btnAddQuestion_Click(object sender, EventArgs e)
        {
            if (!VerificationQuestionFields())
            {
                return;
            }

            AddToListQuestions();
            lbListQuestions.Items.Add(testStructure.Questions[testStructure.Questions.Count - 1].Question);
            ClearQuestionFields();
        }

        private void btnEditQuestion_Click(object sender, EventArgs e)
        {
            if (!VerificationQuestionFields())
            {
                return;
            }

            QuestionStructure question = new QuestionStructure(
                tbQuestion.Text, cbAnswer1.Checked, tbAnswer1.Text, cbAnswer2.Checked, tbAnswer2.Text, cbAnswer3.Checked, tbAnswer3.Text,
                cbAnswer4.Checked, tbAnswer4.Text, cbAnswer5.Checked, tbAnswer5.Text, cbAnswer6.Checked, tbAnswer6.Text, cbUserInputAnswer.Checked
                );
            testStructure.Questions.Insert(lbListQuestions.SelectedIndex, question);
            lbListQuestions.Items.Insert(lbListQuestions.SelectedIndex, testStructure.Questions[lbListQuestions.SelectedIndex].Question);
            testStructure.Questions.RemoveAt(lbListQuestions.SelectedIndex);
            lbListQuestions.Items.RemoveAt(lbListQuestions.SelectedIndex);
            ClearQuestionFields();
        }

        private void btnDeleteQuestion_Click(object sender, EventArgs e)
        {
            testStructure.Questions.RemoveAt(lbListQuestions.SelectedIndex);
            lbListQuestions.Items.RemoveAt(lbListQuestions.SelectedIndex);
            ClearQuestionFields();
        }

        private void cbActivateTimer_CheckedChanged(object sender, EventArgs e)
        {
            EnablingAndDisablingElements();

            if (!cbActivateTimer.Checked)
            {
                tbSetTime.Text = string.Empty;
            }
        }

        private void tbSetTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Char.IsDigit(e.KeyChar) && e.KeyChar != 8;
        }

        private void cbUserInputAnswer_CheckedChanged(object sender, EventArgs e)
        {
            EnablingAndDisablingElements();

            if (cbUserInputAnswer.Checked)
            {
                pQuestion.Controls
                    .OfType<CheckBox>()
                    .Where(c => c.Tag?.ToString() == "CheckedAnswer")
                    .ToList()
                    .ForEach(checkBox => checkBox.Checked = false);
                pQuestion.Controls
                    .OfType<TextBox>()
                    .Where(c => c.Tag?.ToString() == "TextAnswer")
                    .ToList()
                    .ForEach(textBox => textBox.Text = string.Empty);
            }
        }

        private void EnablingAndDisablingElements()
        {
            tbSetTime.Enabled = cbActivateTimer.Checked;
            pQuestion.Controls
                .OfType<CheckBox>()
                .Where(c => c.Tag?.ToString() == "CheckedAnswer")
                .ToList()
                .ForEach(checkBox => checkBox.Enabled = !cbUserInputAnswer.Checked);
            pQuestion.Controls
                .OfType<TextBox>()
                .Where(c => c.Tag?.ToString() == "TextAnswer")
                .ToList()
                .ForEach(textBox => textBox.Enabled = !cbUserInputAnswer.Checked);
        }

        private void ClearQuestionFields()
        {
            pQuestion.Controls.OfType<CheckBox>().ToList().ForEach(checkBox => checkBox.Checked = false);
            pQuestion.Controls.OfType<TextBox>().ToList().ForEach(textBox => textBox.Text = string.Empty);
            lbListQuestions.ClearSelected();
        }

        private void AddToListQuestions()
        {
            QuestionStructure question = new QuestionStructure(
                tbQuestion.Text, cbAnswer1.Checked, tbAnswer1.Text, cbAnswer2.Checked, tbAnswer2.Text, cbAnswer3.Checked, tbAnswer3.Text,
                cbAnswer4.Checked, tbAnswer4.Text, cbAnswer5.Checked, tbAnswer5.Text, cbAnswer6.Checked, tbAnswer6.Text, cbUserInputAnswer.Checked
                );
            testStructure.Questions.Add(question);
        }

        private void AddToListGeneralSettings()
        {
            testStructure.GeneralSettings.Clear();
            GeneralSettings generalSettings = new GeneralSettings(
                tbTopic.Text, cbActivateTimer.Checked,
                tbSetTime.Text, cbShowCorrectOrIncorrectAnswer.Checked
                );
            testStructure.GeneralSettings.Add(generalSettings);
        }

        private void SaveTest()
        {
            sfdSaveTest.Filter = "JSON файлы (*.json)|*.json";
            sfdSaveTest.RestoreDirectory = true;

            if (sfdSaveTest.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }

            File.WriteAllText(sfdSaveTest.FileName, JsonConvert.SerializeObject(testStructure, Formatting.Indented));
        }

        public void OpenTest()
        {
            ofdOpenTest.Filter = "JSON файлы (*.json)|*.json";
            ofdOpenTest.RestoreDirectory = true;

            if (ofdOpenTest.ShowDialog() == DialogResult.Cancel)
            {
                this.Close();
                return;
            }

            testStructure = JsonConvert.DeserializeObject<TestStructure>(File.ReadAllText(ofdOpenTest.FileName));

            foreach (GeneralSettings item in testStructure.GeneralSettings)
            {
                tbTopic.Text = item.Topic;
                cbActivateTimer.Checked = item.ActivateTimer;
                tbSetTime.Text = item.SetTime;
                cbShowCorrectOrIncorrectAnswer.Checked = item.ShowCorrectOrIncorrectAnswer;
            }

            foreach (QuestionStructure item in testStructure.Questions)
            {
                lbListQuestions.Items.Add(item.Question);
            }
        }

        private void btnSaveTest_Click(object sender, EventArgs e)
        {
            if (!VerificationGeneralSettings())
            {
                return;
            }

            AddToListGeneralSettings();
            SaveTest();
            this.Close();
        }

        private void lbListQuestions_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillOutQustionFields();
            btnEditQuestion.Enabled = btnDeleteQuestion.Enabled = lbListQuestions.SelectedIndex >= 0;
        }

        private void FillOutQustionFields()
        {
            if (lbListQuestions.SelectedIndex >= 0)
            {
                tbQuestion.Text = testStructure.Questions[lbListQuestions.SelectedIndex].Question;
                tbAnswer1.Text = testStructure.Questions[lbListQuestions.SelectedIndex].TextAnswer1;
                tbAnswer2.Text = testStructure.Questions[lbListQuestions.SelectedIndex].TextAnswer2;
                tbAnswer3.Text = testStructure.Questions[lbListQuestions.SelectedIndex].TextAnswer3;
                tbAnswer4.Text = testStructure.Questions[lbListQuestions.SelectedIndex].TextAnswer4;
                tbAnswer5.Text = testStructure.Questions[lbListQuestions.SelectedIndex].TextAnswer5;
                tbAnswer6.Text = testStructure.Questions[lbListQuestions.SelectedIndex].TextAnswer6;
                cbAnswer1.Checked = testStructure.Questions[lbListQuestions.SelectedIndex].CheckedAnswer1;
                cbAnswer2.Checked = testStructure.Questions[lbListQuestions.SelectedIndex].CheckedAnswer2;
                cbAnswer3.Checked = testStructure.Questions[lbListQuestions.SelectedIndex].CheckedAnswer3;
                cbAnswer4.Checked = testStructure.Questions[lbListQuestions.SelectedIndex].CheckedAnswer4;
                cbAnswer5.Checked = testStructure.Questions[lbListQuestions.SelectedIndex].CheckedAnswer5;
                cbAnswer6.Checked = testStructure.Questions[lbListQuestions.SelectedIndex].CheckedAnswer6;
                cbUserInputAnswer.Checked = testStructure.Questions[lbListQuestions.SelectedIndex].CheckedUserInputAnswer;
            }
        }

        private bool VerificationGeneralSettings()
        {
            if (tbTopic.Text == string.Empty)
            {
                MessageBox.Show("Поле \"Тема\" не заполнено.",
                    "Предупреждение",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private bool VerificationQuestionFields()
        {
            string errorMessage = "";

            if (tbQuestion.Text == string.Empty)
            {
                errorMessage = "Поле \"Вопрос\" не заполнено.";
            }

            else if ((tbAnswer1.Text == string.Empty || tbAnswer2.Text == string.Empty) && !cbUserInputAnswer.Checked)
            {
                errorMessage = "Не заполнены варианты ответов. Минимальное количество вариантов ответа: 2. ";
            }

            else if ((!cbAnswer1.Checked && !cbAnswer2.Checked && !cbAnswer3.Checked && !cbAnswer4.Checked && !cbAnswer5.Checked && !cbAnswer6.Checked) && !cbUserInputAnswer.Checked)
            {
                errorMessage = "Ни один из вариантов ответов не отмечен как правильный.";
            }

            else if ((tbAnswer3.Text == string.Empty && cbAnswer3.Checked) || (tbAnswer4.Text == string.Empty && cbAnswer4.Checked) || (tbAnswer5.Text == string.Empty && cbAnswer5.Checked) || (tbAnswer6.Text == string.Empty && cbAnswer6.Checked))
            {
                errorMessage = "Нельзя отметить вариант с незаполненным ответом.";
            }

            if ((tbAnswer2.Text != string.Empty && tbAnswer1.Text == string.Empty) || (tbAnswer3.Text != string.Empty && tbAnswer2.Text == string.Empty) || (tbAnswer4.Text != string.Empty && tbAnswer3.Text == string.Empty) || (tbAnswer5.Text != string.Empty && tbAnswer4.Text == string.Empty) || (tbAnswer6.Text != string.Empty && tbAnswer5.Text == string.Empty))
            {
                errorMessage = "Пропущен ответ. Заполняйте ответы по порядку.";
            }

            if (errorMessage != string.Empty)
            {
                MessageBox.Show(errorMessage,
                    "Предупреждение",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
    }
}
